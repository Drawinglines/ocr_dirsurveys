# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 15:19:53 2016

@author: Administrator
"""

# <codecell>
# =============================================================================
# ============== Convert pdf pages to images for processing
# =============================================================================
from wand.image import Image as WandImage
from wand.color import Color
    

def pdf2tif(source_file, target_file="pdf2tiff_temp.tif", dest_width=1895, dest_height=1080):
    
    print 'Converting pdf page to image #####'
    
    #source_file = SourceDir+"\\"+pdf_page_name
    RESOLUTION  = 600
    ret         = True
    img_buffer  = None
    try:
        with WandImage(filename=source_file, resolution=(RESOLUTION,RESOLUTION)) as img:
            img.background_color = Color('white')
            #img_width = img.width
            #ratio     = dest_width / img_width
            #img.resize(dest_width, int(ratio * img.height))
            img.format        = 'tif'
            img.alpha_channel = False
            img.save(filename = target_file)
            
            # Fill image buffer with numpy array from blob
            img_buffer=np.asarray(bytearray(img.make_blob()), dtype=np.uint8)
            if img_buffer is not None:
                retval = cv2.imdecode(img_buffer, cv2.IMREAD_UNCHANGED)
                
    except Exception as e:
        ret = False

    return target_file , retval
'''    
if __name__ == "__main__":
    source_file = ImageDir+"/test1.pdf[44]"
    target_file = ImageDir+"/survey_3.tif"

    ret = pdf2tif(source_file, target_file, 1895, 1080)
'''




# <codecell>
import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.filters import  threshold_adaptive
from skimage.filters.rank import median
from skimage.morphology import disk

import os
Figdir = 'Fig_Loc'
if not os.path.exists(Figdir):
    os.makedirs(Figdir)






# =============================================================================
# ============== Extracting Table GridLines
# =============================================================================
#%% extract the gridlines function
def Extract_GridLines(color_npArray):
    
    #%% convert image to gray
    Im_gray = cv2.cvtColor(color_npArray, cv2.COLOR_BGR2GRAY)
    showImage(Im_gray,'gray')
    
    
    #fig_loc = '%s/Orig_gray.tiff' % (Figdir)
    #cv2.imwrite(fig_loc, Im_gray)

    #%% deskew the document
    #Im_gray = deskew(Im_gray.copy())
    img_aligned, scores, _ = align_grid(Im_gray.copy(), angle_range=np.linspace(0, 1, 10))
    showImage(img_aligned,'gray')
    
    
    #fig_loc = '%s/aligned_gray.tiff' % (Figdir)
    #cv2.imwrite(fig_loc, img_aligned)
    
    
    
 
    #%% crop the table area only
    img_gray_cropped , hough_lines = extract_grid(img_aligned.copy(), linel=100, linelink=1, grid_reg_thr=2)
    #showImage(img_gray_cropped,'gray')

    
    #fig_loc = '%s/cropped_gray.tiff' % (Figdir)
    #cv2.imwrite(fig_loc, img_gray_cropped)
    
    
    
    
    
  
    
    
    


    #%% binarize image
    img_bin = Reza_binarization(img_gray_cropped.copy(),32,64,64)
    #showImage(img_bin,'gray')

    #%% Hough transform for line extraction

    #%% get horiontal lines
    HoughPP_lines_orig,_ = Reza_HoughLines_2(img_bin.copy(), \
                        sobel_filter_x=0 , sobel_filter_y=1 , sobel_type = cv2.CV_64F , \
                        morph_close_kernel=(10,1) , morph_open_kernel=(300,1), \
                        hough_link=100 , hough_gap=1)
    #===== remove every other line and only keep horizontal ones
    Hough_lines_horiz = filter_houghlines(HoughPP_lines_orig , 0.)
    
    
    
    
    
    #%% Vertical Lines
    HoughPP_lines_orig,_ = Reza_HoughLines_2(img_bin.copy(), \
                        sobel_filter_x=1 , sobel_filter_y=0 , sobel_type = cv2.CV_64F , \
                        morph_close_kernel=(1,20) , morph_open_kernel=(1,150), \
                        hough_link=100 , hough_gap=1)
    #===== remove every other line and only keep horizontal ones
    Hough_lines_vert = filter_houghlines(HoughPP_lines_orig , np.pi/2.)
    
    
    #%% remove lines from binary image
    Hough_lines = np.concatenate((Hough_lines_horiz,Hough_lines_vert),axis=0)
    rmv_img_bin = Remove_Lines_From_Image(img_bin.copy() , Hough_lines)
    #showImage(rmv_img_bin,'gray')
    
    #==== clean the binary image
    kernel_close    = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
    clean_img_bin   = cv2.morphologyEx(rmv_img_bin.copy(), cv2.MORPH_CLOSE, kernel_close, iterations=1)
    #showImage(clean_img_bin,'gray')
    
    
    #%% show hough lines
    temp_img = Remove_Lines_From_Image(img_gray_cropped.copy() , Hough_lines) 
    showImage(temp_img,'gray')
    
    #fig_loc = '%s/Bin_removedAllLines_gray.tiff' % (Figdir)
    #cv2.imwrite(fig_loc, temp_img)
    
    
    #%%===== clean up the hough lines to be used for extracting cells for OCR
    Hough_lines_vert  = cleanHoughLines_Threshold(Hough_lines_vert, thr=10.)
    Hough_lines_horiz = cleanHoughLines_Threshold(Hough_lines_horiz, thr=10.)
    
    
    #==== add border lines 
    (my,mx)=img_bin.shape
    Hough_lines_horiz = np.concatenate(([[0.,0.]],Hough_lines_horiz,[[my,0.]]),axis=0)
    Hough_lines_vert  = np.concatenate(([[0.,np.pi/2.]],Hough_lines_vert,[[mx,np.pi/2.]]),axis=0)
    
    
    
    Hough_lines       = np.concatenate((Hough_lines_horiz,Hough_lines_vert),axis=0)
    
    #%% show hough lines
    temp_img = Remove_Lines_From_Image(img_gray_cropped.copy() , Hough_lines) 
    showImage(temp_img,'gray')
    
    
    #fig_loc = '%s/Bin_removedCleanedLines_gray.tiff' % (Figdir)
    #cv2.imwrite(fig_loc, temp_img)
    
    
    
    return Hough_lines , Hough_lines_vert , Hough_lines_horiz , clean_img_bin
    
    
# ===========================
def sliding_window(image, stepSize, windowSize=(32,32)):
	# slide a window across the image
	for y in xrange(0, image.shape[0], stepSize):
		for x in xrange(0, image.shape[1], stepSize):
			# yield the current window
			yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])
   
def Reza_binarization(gray_image,stepSize,winW,winH):
    bin_image = gray_image.copy()
    for (x, y, windw) in sliding_window(gray_image, stepSize, windowSize=(winW, winH)):
		# if the window does not meet our desired window size, ignore it
        '''        
        if windw.shape[0] != winH or windw.shape[1] != winW:
    		continue
        '''
        
        # THIS IS WHERE YOU WOULD PROCESS YOUR WINDOW, SUCH AS APPLYING A
		# MACHINE LEARNING CLASSIFIER TO CLASSIFY THE CONTENTS OF THE
		# WINDOW
        windw = np.longfloat(windw)
        mn = np.mean(windw) 
        std= np.std(windw)
        img_w,img_h = np.shape(windw)
        NP = img_w*img_h
        
        '''
        #=== Niblack algorithm
        k = -0.2
        Calc_Threshold = mn + k*np.sqrt(np.sum(np.power(windw,2))/NP-mn**2)
        '''
        
        
        #=== NICK algorithm
        k = -0.060
        Calc_Threshold = mn + k*np.sqrt((np.sum(np.power(windw,2))-mn**2)/NP)
        
        
        '''
        #=== Wolf's Algorithm
        k = 0.5
        Calc_Threshold = (1-k)*mn+k*M+k*s/R*(m-M)
        '''
        
        
        #==== Threshold the window
        _,Bin_window = cv2.threshold(np.uint8(windw.copy()), Calc_Threshold , 255, cv2.THRESH_BINARY) 
        bin_image[y:y+winH,x:x+winW] = Bin_window
        
    return bin_image
   
   
   
    
    
def align_grid(img, angle_range=np.linspace(0, 1, 10), linel=300, linew=1):
    """ Find the orientation of the image which contains the 
    largest number of long vertical lines. """
    print 'Finding best angle for de-skewing ####'
    rows, cols = img.shape        
    kernelx = cv2.getStructuringElement(cv2.MORPH_RECT, (linew,linel))
    kernely = cv2.getStructuringElement(cv2.MORPH_RECT, (linel,linew))
    best_erode = 0
    best_angle = None
    scores = []

    for angle_id, angle in enumerate(angle_range):
        M     = cv2.getRotationMatrix2D((cols/2, rows/2), angle+1e-3, 1)
        img_r = 255-cv2.warpAffine(img, M, (cols, rows), borderMode=cv2.BORDER_CONSTANT, borderValue=255)
        
        
        dx = cv2.Sobel(img_r, cv2.CV_16S, 1, 0) # for vertical lines
        dx = cv2.convertScaleAbs(dx)
        erodex = cv2.morphologyEx(dx, cv2.MORPH_OPEN, kernelx, iterations=1)
        
        dy = cv2.Sobel(img_r, cv2.CV_16S, 0, 1) # for horizontal lines
        dy = cv2.convertScaleAbs(dy)
        erodey = cv2.morphologyEx(dy, cv2.MORPH_OPEN, kernely, iterations=1)
        
        scores.append(erodex.sum()+erodey.sum())
        #print('Iter {}/{}. Angle {:.2f}, score {}.'.format(angle_id, len(angle_range),
        #                                                   angle, scores[-1]))
        if best_erode < scores[-1]:
            best_erode = scores[-1]
            best_angle = angle
    
    print('Best Anlge is : {:.2f} '.format(best_angle))
    
    M     = cv2.getRotationMatrix2D((cols/2, rows/2), best_angle, 1)
    img_r = cv2.warpAffine(img, M, (cols, rows),  borderMode=cv2.BORDER_CONSTANT, borderValue=255)
    return img_r , scores , best_angle
    


def extract_grid(gray_img, linel=100, linelink=1, grid_reg_thr=2):
    cropped_gray_img = gray_img.copy()
    
    # extract lines
    img_bin = Reza_binarization(gray_img.copy(),32,64,64)
    
    Hough_lines_orig,_ = Reza_HoughLines_2(img_bin.copy(), \
                        sobel_filter_x=0 , sobel_filter_y=1 , sobel_type = cv2.CV_64F , \
                        morph_close_kernel=(10,1) , morph_open_kernel=(300,1), \
                        hough_link=linel , hough_gap=linelink)
    
    
    #===== remove every other line and only keep horizontal ones
    Hough_lines = filter_houghlines(Hough_lines_orig , 0.)
    
    
    #===== clean up the hough lines
    Hough_lines = cleanHoughLines_Threshold(Hough_lines, thr=10.)
    #Hough_lines,_,bandwidth = cleanHoughLines_MeanShift(Hough_lines,prcnt=97.5)
    #Hough_lines  = np.array(cleanHoughLines(Hough_lines , 40.)) 
    
    

    #===== extract location of horizontal lines
    #lines = Hough_lines[:,0] 
    
    
    
    
    
    
    #=== Get the Proper Zone
    diffs = np.diff(Hough_lines,axis=0)
    aa,lables,_ = cleanHoughLines_MeanShift(diffs,prcnt=90)
    selected_indexes = find_largest_subseq(lables)
    yseg_start = Hough_lines[selected_indexes[0],0]
    yseg_end   = Hough_lines[selected_indexes[-1]+1,0]
    cropped_gray_img = gray_img[int(yseg_start):int(yseg_end),:] # crop the image
    return cropped_gray_img , Hough_lines





def find_largest_subseq(s_array):
    
    sel_indx=[]
    segments=[]
    subseq_len=0
    for i,s in enumerate(s_array):
        
        if i<len(s_array)-1:
            if s==s_array[i+1]:
                sel_indx.append(i)
                subseq_len +=1
            else:
                sel_indx.append(i)
                segments.append(sel_indx)
                sel_indx=[]
                subseq_len=0
    
    lengs = [len(s) for s in segments]
    max_seg = segments[np.argmax(lengs)]
            
    return max_seg        
                
        
            

def indx_sort(s):
    sorted_indx = sorted(range(len(s)), key=lambda k: s[k])
    sorted_array= s[sorted_indx]
    return sorted_array , s , sorted_indx

    
    

def polar2z(r,theta):
    return r * np.exp( 1j * theta )

def z2polar(z):
    return ( np.abs(z), np.angle(z) )   
    
# <codecell>
import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage import exposure
import scipy.cluster.hierarchy as hcluster
import Reza_pytesser as pytesser
import os
from skimage.morphology import disk
from skimage.filters.rank import median
# =============================================================================
# ============== Image Processing Modules Used on Other modules
# =============================================================================
#%% Simple enhancement operations
def Hist_Stretching(Im_gray):
    # Contrast stretching
    p2, p98 = np.percentile(Im_gray, (10, 90))
    img_rescale = exposure.rescale_intensity(Im_gray, in_range=(p2, p98))
    return img_rescale

def showImage(inputImage,colormap):

    if colormap=="color":
        plt.figure().add_subplot(111).imshow(cv2.cvtColor(inputImage, cv2.COLOR_BGR2RGB))
    else:
        plt.figure().add_subplot(111).imshow(inputImage,colormap)

def show_image2(img,colormap="color",sbplt=[1,1,1],figsize=(20,20)):
    
    plt.figure(figsize=figsize)    
    plt.subplot(sbplt[0],sbplt[1],sbplt[2])
    if colormap=="color":
        tmp_img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        plt.imshow(tmp_img)   
    else:
        plt.imshow(img,colormap)   
    
    return plt
         

#%% Feature extraction operations
def auto_canny(image, sigma=0.33):
	# compute the median of the single channel pixel intensities
	v = np.median(image)
 
	# apply automatic Canny edge detection using the computed median
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper,apertureSize = 3 , L2gradient =True)
 
	# return the edged image
	return edged


#%%

def Reza_HoughLines_2(img_bin, sobel_filter_x = 0 , sobel_filter_y = 1 , sobel_type = cv2.CV_64F , morph_close_kernel=(10,1) , morph_open_kernel=(300,1),hough_link=100 , hough_gap=1):
    

    
    #==== directional lines
    dy = cv2.Sobel(img_bin.copy(),sobel_type,sobel_filter_x,sobel_filter_y,ksize=5)
    dy = cv2.convertScaleAbs(dy)
    #showImage(dy,'gray')
    
    
    #====== clean up lines and connect them
    kernel_close  = cv2.getStructuringElement(cv2.MORPH_RECT, morph_close_kernel)
    dy            = cv2.morphologyEx(dy, cv2.MORPH_CLOSE, kernel_close, iterations=1)
    kernel_open  = cv2.getStructuringElement(cv2.MORPH_RECT, morph_open_kernel)
    dy           = cv2.morphologyEx(dy, cv2.MORPH_OPEN, kernel_open, iterations=1)
    #showImage(dy,'gray')
    
    
    # take hough transform
    HoughPP_lines = cv2.HoughLinesP(dy, 1, np.pi/2., 2, None, hough_link, hough_gap)
    
    
    # Store the y-coords of horizontal lines. only horizontal lines are selected
    angles = []
    lines  = []
    for line in HoughPP_lines:
        x1,y1,x2,y2=line[0]
        
        if x2==x1: # vertical line
            angle     = np.pi/2. 
            orig_dist = x2 
        else: # non vertical line, calc radial coordinates like ordinary lines
            slope = np.float64(y2-y1)/np.float64(x2-x1)
            angle = np.arctan(slope)
            orig_dist = min_orig_dist(slope,x1,y1)
        
        # store line locations and angles
        lines.append(orig_dist)
        angles.append(angle)
        
    # put in format of hough lines [location , angle]        
    Hough_lines = np.array(zip(lines,angles), dtype=np.float64)
    Hough_lines = np.sort(Hough_lines,axis=0)
    
    return Hough_lines , HoughPP_lines
    
    
    
def min_orig_dist(m,x1,y1):
    # finds the minimum distance from origin given a line passing (x1,y1) with slope m    
    x_opt = np.float64(m*np.float64(m*x1-y1))/np.float64(m**2+1.)
    y_opt = y1+m*np.float64(x_opt-x1)
    orig_dist = np.sqrt(x_opt**2+y_opt**2)
    
    return orig_dist
    
#%%  
def filter_houghlines(hough_lines , angle):
    # filters hough lines with specific angle
    indxs = [i for i,s in enumerate(hough_lines) if (s[1]-angle)<=1e-3]
    new_hough_lines = hough_lines[indxs,:]
    return new_hough_lines
    
def cleanHoughLines_Threshold(hough_lines, thr=10.):
    lines = hough_lines[:,0]
    diffs = np.diff(lines)
    indx  = np.where(diffs>thr)[0]
    hough_lines = hough_lines[indx,:]
    return hough_lines
    
def cleanHoughLines(X,thresh=40.0):
    
    # clustering
    cluster_lables = hcluster.fclusterdata(X, thresh, criterion="maxclust",method="average") # distance maxclust #centroid complete
    UniqueClusters = np.unique(cluster_lables)
    
    Clustermeans_X =[]
    for i in UniqueClusters:
        #Clustermeans_X.append(np.percentile(X[cluster_lables==i],0.8,0)) # could use mean med or any other statistics
        Clustermeans_X.append(np.mean(X[cluster_lables==i],0)) # could use mean med or any other statistics

    return Clustermeans_X
    
    
def cleanHoughLines_MeanShift(in_houghlines,prcnt=98):
    X = in_houghlines
    
    from sklearn.cluster import MeanShift, estimate_bandwidth
    #X = np.array(zip(line_ys,angle_ys), dtype=np.float64)
    
    #bandwidth = estimate_bandwidth(X, quantile=0.03)
    bandwidth = np.percentile(np.diff(X[:,0],n=1),prcnt)
    #print "bandwidth for meanshift: " + str(bandwidth)
    
    ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
    ms.fit(X)
    labels = ms.labels_
    cluster_centers = ms.cluster_centers_
    
    labels_unique = np.unique(labels)
    #n_clusters_   = len(labels_unique)
    cluster_centers=[]
    for i in labels_unique:
            #Clustermeans_X.append(np.percentile(X[cluster_lables==i],0.8,0)) # could use mean med or any other statistics
            cluster_centers.append(np.mean(X[labels==i,:],0))      # could use mean med or any other statistics
    cluster_centers = np.array(cluster_centers)
    cluster_centers = np.sort(cluster_centers,axis=0)
    
    # change data types
    #line_ys    = np.asarray(cluster_centers[:,0],np.int64)
    #angle_ys   = np.asarray(cluster_centers[:,1],np.float64)
    #houghlines_clustered = zip(line_ys,angle_ys)
    
    #aa = np.diff(line_ys)
    return cluster_centers , labels , bandwidth
###############################################################################    

    
def Remove_Lines_From_Image(bin_img , lines):
    Im_New = bin_img.copy()
    for line in lines:
        
        
        rho,theta  = line
        
        a = np.sin(theta)
        b = np.cos(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 10000*(-b))
        y1 = int(y0 + 10000*(a))
        x2 = int(x0 - 10000*(-b))
        y2 = int(y0 - 10000*(a))
    
        cv2.line(Im_New,(x1,y1),(x2,y2),(255,255,255),10) # color  , thickness of line
        
    return Im_New
# <codecell>   
import itertools
# =============================================================================
# ============== Optical Character Recognition OCR Tools
# =============================================================================    
#%% OCR a single cell provided by image and horizontal and vertical lines
def cell_OCR(Im_orig , vertical, horizontal):
    
    
    # sort the lines
    vertical   = np.sort(vertical,0)
    horizontal = np.sort(horizontal,0)
    # go through each horizontal line (aka row)
    
    table=[]
    shp = np.shape(Im_orig)
    big_image = 255.0*np.ones((shp[0],shp[1]),dtype=np.uint8)
    for i, h in enumerate(horizontal):
        if i < len(horizontal)-1:
            row = []
            for j, v in enumerate(vertical):
                if i < len(horizontal)-1 and j < len(vertical)-1:
                    # every cell before last cell
                    # get width & height
                    width  = horizontal[i+1][0] - h[0]
                    height = vertical[j+1][0] - v[0]

                else:
                    # last cell, width = cell start to end of image
                    # get width & height
                    #width  = 1
                    #height = 1
                    width  = np.shape(Im_orig)[0] - h[0]
                    height = np.shape(Im_orig)[1] - v[0]
                
                if width<=3 or height<=3:
                    continue
                    #roi = np.zeros((8,8,3),dtype=np.uint8)
                
                    

                # get roi (region of interest) to find an x
                roi = Im_orig[int(h[0]):int(h[0]+width), int(v[0]):int(v[0]+height)]

                
                '''
                if i>=5:
                    continue
                '''
               
                    
                
                '''
                if i==10 and j==11:
                    print "hi"
                else:
                    continue
                '''
                
                txtocr   =[]
                OCRed_img=[]
                filterLevels = [[7],[3],[140]]
                filterLevels = list(itertools.product(*filterLevels))
                for diskdim,kerneldim,pyrsize in filterLevels:
                
                    OCRPreProced_binimg = PreProcess_ImageCell_For_OCR(roi,diskdim,kerneldim,pyrsize)
                    txt                 = pytesser.multi_image_to_strings([OCRPreProced_binimg] , cleanup = True)
                     
                    #print txt
                    #==== OCR with Tesseract
                    OCRed_img.append(OCRPreProced_binimg)
                    txtocr.append(txt[0])
                
                
                
                #uniq_txtocr = list(set(txtocr))
                #uniq_conts = [txtocr.count(s) for ix,s in enumerate(uniq_txtocr)]
                orig_conts = [txtocr.count(s) for ix,s in enumerate(txtocr)]
                
                
                
                #==== calc the weights for ocred texts
                
                alpha_weights=[]
                for ix,t in enumerate(txtocr):
                    if t.isalpha()==True or t.isdigit()==True: # or t=='':
                        alpha_weights.append(2)
                    else:
                        alpha_weights.append(1)
                orig_conts = np.multiply(alpha_weights,orig_conts)
                   
                
                #==== find the max lik solution
                indx  = np.argmax(orig_conts)
                
                txt   = txtocr[indx]
                #txt = long_substr(txtocr)
                #txt = lcs(txtocr)
                
                
                #print "min counts: "+str(np.min(uniq_conts))
                #print "max counts: "+str(np.max(uniq_conts))
                #print uniq_conts 
                #print uniq_txtocr
                #print "selected scale: "+str(filterLevels[indx])
                #print "selected OCR: " + txt
                
                
                
                
                annot_img = OCRed_img[indx].copy()
                img_ocr   = OCRed_img[indx].copy()
                
                
                
                #===== put the text on image
                cv2.putText(annot_img,txt, (20,20),cv2.FONT_HERSHEY_SIMPLEX, 1,(0,0,255),2)
                    
                dir = 'table%s' % (1)
                if not os.path.exists(dir):
                       os.makedirs(dir)
                fn1 = '%s/roi_r%s-c%s.tiff' % (dir, i, j)
                
                dir = 'table%s' % (2)
                if not os.path.exists(dir):
                       os.makedirs(dir)
                fn2 = '%s/roi_r%s-c%s.tiff' % (dir, i, j)
                
                #if txt!='':
                cv2.imwrite(fn1, annot_img)
                cv2.imwrite(fn2, img_ocr )
                
                
                
                #===== save the processed ocr ready big image in hard
                big_image[int(h[0]):int(h[0]+width), int(v[0]):int(v[0]+height)] = img_ocr.copy()
                dir = 'Figures%s' % (1)
                if not os.path.exists(dir):
                       os.makedirs(dir)
                loc = '%s/bigimg.tiff' % (dir)
                
                #===== Put the retrieved txt into a list of lists to be written as csv file
                #===== each row is a list in the list of lists
                row.append(txt)
              
        table.append(row)
    
    #=== save the big proccessed image in hard drive
    cv2.imwrite(loc, big_image)                     
    return table
                    


                
#%% Tools for trimming and enhancing for ocr module               
def whiten_borders(img , x0thik=0, x1thik=0, y0thik=0, y1thik=0):
    img_out = img.copy()
    (hs,ws) = np.shape(img)
    
    if ~(hs<(x0thik+x1thik+8) or ws<(y0thik+y1thik+8)):
        # whiten the around area
        img_out[:x0thik-1,:] = 255
        img_out[1-x1thik:,:] = 255
        img_out[:,:y0thik-1] = 255
        img_out[:,1-y1thik:] = 255
        
    return img_out
         
def PreProcess_ImageCell_For_OCR(roi,diskdim,kerneldim=4,threshold_bin=110):
     out_Grey_ImageNpArray = roi.copy()
     thresh = roi.copy()
     
     #roi         = Background_subtraction(roi.copy())
    
     #roi_gry     = cv2.cvtColor(roi.copy(), cv2.COLOR_BGR2GRAY)
     #threshimg   = roi_gry.copy()
     #ret, thresh = cv2.threshold(threshimg.copy(), threshold_bin , 255, cv2.THRESH_BINARY) 
     
     
     
     #thresh      = whiten_borders(thresh.copy() , x0thik=40,x1thik=20 , y0thik=40, y1thik=5)
     thresh      = median(thresh.copy(), disk(diskdim)) 
     #==== clean the binary image
     #kernel_close    = cv2.getStructuringElement(cv2.MORPH_RECT, (10,10))
     #thresh          = cv2.morphologyEx(thresh.copy(), cv2.MORPH_OPEN, kernel_close, iterations=1)
     
     #=== erode the text (make thicker)
     '''
     kernel = np.ones((kerneldim,kerneldim),np.uint8)
     eroded = cv2.erode(thresh.copy(),kernel,iterations = 2)
     thresh = eroded.copy()
     '''
     
     
     '''
     Im_autocanny = auto_canny(thresh.copy())
     kernel       = cv2.getStructuringElement(cv2.MORPH_CROSS,(30,30))
     dilated      = cv2.dilate(Im_autocanny,kernel,iterations = 10) # dilate
     im2, contours, hierarchy = cv2.findContours(dilated,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
     
     try:
         cnt     = contours[0]
         x,y,w,h = cv2.boundingRect(cnt)
         temp = np.uint8(255.*np.ones(np.shape(thresh)))
         temp[y:y+h,x:x+w]  = thresh[y:y+h,x:x+w]
     except:
         "just take the threshold image"
     
     '''
     
     out_Grey_ImageNpArray = thresh.copy()
     return out_Grey_ImageNpArray
                   
       



            
def long_substr(data):
    substr = ''
    if len(data) > 1 and len(data[0]) > 0:
        for i in range(len(data[0])):
            for j in range(len(data[0])-i+1):
                if j > len(substr) and is_substr(data[0][i:i+j], data):
                    substr = data[0][i:i+j]
    return substr

def is_substr(find, data):
    if len(data) < 1 and len(find) < 1:
        return False
    for i in range(len(data)):
        if find not in data[i]:
            return False
    return True

def calc_cache_pos(strings, indexes):
    factor = 1
    pos = 0
    for s, i in zip(strings, indexes):
        pos += i * factor
        factor *= len(s)
    return pos

def lcs_back(strings, indexes, cache):
    if -1 in indexes:
        return ""
    match = all(strings[0][indexes[0]] == s[i]
                for s, i in zip(strings, indexes))
    if match:
        new_indexes = [i - 1 for i in indexes]
        result = lcs_back(strings, new_indexes, cache) + strings[0][indexes[0]]
    else:
        substrings = [""] * len(strings)
        for n in range(len(strings)):
            if indexes[n] > 0:
                new_indexes = indexes[:]
                new_indexes[n] -= 1
                cache_pos = calc_cache_pos(strings, new_indexes)
                if cache[cache_pos] is None:
                    substrings[n] = lcs_back(strings, new_indexes, cache)
                else:
                    substrings[n] = cache[cache_pos]
        result = max(substrings, key=len)
    cache[calc_cache_pos(strings, indexes)] = result
    return result

def lcs(strings):
    """
    >>> lcs(['666222054263314443712', '5432127413542377777', '6664664565464057425'])
    '54442'
    >>> lcs(['abacbdab', 'bdcaba', 'cbacaa'])
    'baa'
    """
    if len(strings) == 0:
        return ""
    elif len(strings) == 1:
        return strings[0]
    else:
        cache_size = 1
        for s in strings:
            cache_size *= len(s)
        cache = [None] * cache_size
        indexes = [len(s) - 1 for s in strings]
        return lcs_back(strings, indexes, cache)                   
                   
#%% convert image types
from PIL import Image
def PIL2OpenCVarray(PILimg):
    pil_image     = PILimg.convert('RGB')
    open_cv_image = np.array(pil_image) 
    # Convert RGB to BGR 
    open_cv_image = open_cv_image[:, :, ::-1].copy() 
    return open_cv_image

def OpenCVarray2PIL(OPENCVimg):
    trueimg = cv2.cvtColor(OPENCVimg, cv2.COLOR_BGR2RGB)
    PILimg = Image.fromarray(trueimg)
    
    return PILimg
    
#%% Tools for storing the results in the memory
import csv
def Write_to_CSV(list_of_lists , name="temp"):
    
    with open(name, "ab") as f:
        writer = csv.writer(f,delimiter=',',dialect='excel')
        writer.writerows(list_of_lists)
    
    
#%%
