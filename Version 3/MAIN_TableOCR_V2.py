# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 15:13:06 2016

@author: Administrator
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
plt.close("all")

import Reza_OCR_Helpers as OCR_hp


# <codecell>
# =============================================================================
# ==== Initialization
ImageDir = "..\\data"
pdfnames  = ["test1.pdf"]
pagesnums = range(42,43)

#list_of_pdfs = [pdfnames+"["+str(s)+"]" for s in pagesnums]
#list_of_pdfs = ["test1.pdf[44]"]


#%%
def Digitize_Directional_Surveys(args):
    OCR_table = False
    # ===== Extract the arguments
    pdf_page_name = args[0]
    csv_page_name = args[1]
    source_file   = ImageDir+"\\"+pdf_page_name
    
    # ====== convert page to image
    target_file = "pdf2tiff_temp_2.tif"
    #target_file = "Untitled.png"
    target_file , orig_color_npArray =  OCR_hp.pdf2tif(source_file,target_file)
    
    
    
    # ===== load the saved image
    orig_color_npArray = cv2.imread(target_file,1)
    OCR_hp.showImage(orig_color_npArray,'color')
    
    
    
    fig_loc = 'orig_color_img.tiff'
    cv2.imwrite(fig_loc, orig_color_npArray)
    
    
    # ===== extract horizontal and vertical lines
    
    lines, lines_vert , lines_horiz , cropped_img_bin = OCR_hp.Extract_GridLines(orig_color_npArray)
    
    #=== Get the proper zone containing the table
    #lines_horiz,lines_vert = OCR_hp.Crop_Adaptive_Image(lines_horiz , lines_vert) 
    
    

    # ===== Remove Lines from the image
    #lines = np.concatenate((lines_vert , lines_horiz),axis=0)
    #New_color_npArray = OCR_hp.Remove_Lines_From_Image(orig_color_npArray , lines)
    
    
    # ====== OCR each cell and get the full csv table
    OCR_hp.showImage(cropped_img_bin,'gray')
    OCR_table = OCR_hp.cell_OCR( cropped_img_bin , lines_vert, lines_horiz)
    
    
    
    # ====== 
    OCR_hp.Write_to_CSV(OCR_table , name=csv_page_name)

    return OCR_table


# <codecell>
# =============================================================================
# ================ Get lines of the
# =============================================================================
import csv
for pdfFile in pdfnames:
    print "File: "+pdfFile
    
    
    # creat the survey csv file
    csvFile = pdfFile[:pdfFile.find(".")]+".csv" # remove .pdf and assing .csv to the name
    with open(csvFile, "wb") as f:
        writer = csv.writer(f)
    
    
    
    # OCR pages    
    for pagenum in pagesnums:
        print "Page number: "+str(pagenum)
        arg       = (pdfFile+"["+str(pagenum)+"]" ,csvFile) # first arg: loc of file to ocr //// second: csv file 
        Final_OCR = Digitize_Directional_Surveys(arg)




