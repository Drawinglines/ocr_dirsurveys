# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 10:31:52 2016

@author: Administrator
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
import ImageOCR_Reza_helpers as OCR_hp
from skimage.filters import threshold_otsu, threshold_adaptive
from skimage.filters.rank import median
from skimage.morphology import disk

                              
plt.close("all")

ImageDir = "C:/Users/Administrator/Desktop/Reza Projects/OCR Well Prod/Proddata_digitization/tests1"


        
def binarize_image(roi,diskdim=8,kerneldim=4,pyrsize=0):
     out_Grey_ImageNpArray = roi.copy()
     
     roi_gry     = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
     #thresh = roi_gry.copy()
     #thresh1     = roi_gry.copy()
     thresh     = OCR_hp.Hist_Stretching(roi_gry)
     
     #thresh      = imclearborder(thresh1, diskdim)
     #thresh      = median(thresh1, disk(diskdim))
     ret, thresh = cv2.threshold(thresh.copy(), 30 , 255, cv2.THRESH_BINARY)  
     thresh      = median(thresh.copy(), disk(diskdim))
     #thresh      = np.uint8(255.0*(threshold_adaptive(thresh, block_size = 45, method ='mean' , offset=20)))
     
     #=== erosion to make the numbers and text thinner 
     kernel = np.ones((kerneldim,kerneldim),np.uint8)
     #eroded = cv2.dilate(thresh.copy(),kernel,iterations = 1)
     eroded = cv2.erode(~thresh.copy(),kernel,iterations = 1)
     thresh = ~eroded.copy()

     #thresh      = median(thresh.copy(), disk(3))
 
     out_Grey_ImageNpArray = thresh.copy()
     #del thresh , thresh1 , roi_gry
     return out_Grey_ImageNpArray
     
    
#==============================================================================
#== load the original image
Im_orig = cv2.imread(ImageDir+'/survey_3.tif',1)
OCR_hp.showImage(Im_orig,'color')

'''
#=== enhance image
enhancer = ImageEnhance.Sharpness(OCR_hp.OpenCVarray2PIL(Im_orig))
newImage0 = enhancer.enhance(0.01)

Im_orig = OCR_hp.PIL2OpenCVarray(newImage0)
OCR_hp.showImage(Im_orig,'color')
'''


#== get the gray scale image
Im_gray = cv2.cvtColor(Im_orig, cv2.COLOR_BGR2GRAY)
#Im_gray    = cv2.imread(ImageDir+'/survey_2.tif',cv2.IMREAD_GRAYSCALE  ) # cv2.IMREAD_GRAYSCALE
OCR_hp.showImage(Im_gray,'gray')




#== threshold image to darken lines
#Im_gray = OCR_hp.Hist_Stretching(Im_gray)
Im_bin       = np.uint8(threshold_adaptive(Im_gray, block_size = 45, offset=15))
Im_bin_txt   = median(Im_bin, disk(8))
Im_bin_table = Im_bin-Im_bin_txt
OCR_hp.showImage(Im_bin_table,'gray')


#== filter image to smooth
#Im_blurred = Im_gray.copy()
#Im_blurred = cv2.GaussianBlur(Im_gray, (3, 3), 0)
#blurred   = cv2.medianBlur(img,5)
#showImage(Im_blurred,'gray')


#== Edge detection
Im_autocanny=Im_bin_table
#Im_autocanny = OCR_hp.auto_canny(Im_gray)
#OCR_hp.showImage(Im_autocanny,'gray')



'''
#==== Opening and closing to remove white points and connect the lines respectively
kernel  = np.ones((5,5),np.uint8)
#opening = cv2.morphologyEx(Im_autocanny, cv2.MORPH_OPEN, kernel)
closing = cv2.morphologyEx(Im_autocanny, cv2.MORPH_CLOSE, kernel)
#opening = cv2.morphologyEx(Im_autocanny, cv2.MORPH_OPEN, kernel)
Im_autocanny = closing

OCR_hp.showImage(Im_autocanny,'gray')


'''




'''
newImg,rows = OCR_hp.hough_transform_p(Im_autocanny,Im_orig)
OCR_hp.showImage(newImg,'color')
'''

'''
from skimage.feature import hog
from skimage import  exposure
fd, hog_image      = hog(Im_autocanny, orientations=8, pixels_per_cell=(10, 10),cells_per_block=(1, 1), visualise=True)
hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))     
OCR_hp.showImage(hog_image_rescaled,'gray')
'''               
#== hough transform to extract the lines
#lines_vert = OCR_hp.Reza_houghLines(Im_autocanny,rho_res=1,theta_res=np.pi,hough_trsh=500,clean_trsh=40.0,linetype='vert')
#lines_horiz = OCR_hp.Reza_houghLines(Im_autocanny,rho_res=1,theta_res=np.pi/2.0,hough_trsh=50,clean_trsh=42,linetype='horiz')

lines_vert  = OCR_hp.Reza_houghLines(Im_autocanny,rho_res=1,theta_res=np.pi,hough_trsh=450,clean_trsh=30,linetype='vert')
lines_horiz = OCR_hp.Reza_houghLines(Im_autocanny,rho_res=1,theta_res=np.pi/2.0,hough_trsh=400,clean_trsh=30,linetype='horiz')
#lines = lines_horiz



#=== Get the proper zone containing the table
lines_horiz,lines_vert = OCR_hp.Crop_Adaptive_Image(lines_horiz , lines_vert)
lines = np.concatenate((lines_vert , lines_horiz),axis=0)



#====== binarize the image and save to hard
_,tempimg = cv2.threshold(cv2.cvtColor(Im_orig.copy(), cv2.COLOR_BGR2GRAY), 170 , 255, cv2.THRESH_BINARY)
#cv2.imwrite('tesat_table.png',tempimg)



#=== plot the lines on image overlayed           
Im_hough = Im_orig.copy()
Im_hough2=tempimg.copy()
for line in lines:
    rho,theta  = line
    #print rho 
    #print theta
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho
    x1 = int(x0 + 10000*(-b))
    y1 = int(y0 + 10000*(a))
    x2 = int(x0 - 10000*(-b))
    y2 = int(y0 - 10000*(a))

    cv2.line(Im_hough,(x1,y1),(x2,y2),(255,255,255),20)
    cv2.line(Im_hough2,(x1,y1),(x2,y2),(255,255,255),20)
    

#cv2.imwrite(ImageDir+'/houghlines3.jpg',Im_hough)
OCR_hp.showImage(Im_hough,'color')






#====== ocr the image$
OCR_table = OCR_hp.cell_OCR( Im_hough , lines_vert, lines_horiz)



print "hi"


