"""OCR in Python using the Tesseract engine from Google
http://code.google.com/p/pytesser/
by Michael J.T. O'Kelly
V 0.0.1, 3/10/07"""

import Image
import subprocess
from string import whitespace
import numpy as np

import util
import errors

tesseract_exe_name = "C:\\Program Files (x86)\\Tesseract-OCR\\tesseract.exe" # Name of executable to be called at command line
scratch_image_name = "temp.png" # This file must be .bmp or other Tesseract-compatible format
scratch_text_name_root = "temp" # Leave out the .txt extension
cleanup_scratch_flag = True  # Temporary files cleaned up after OCR operation


def multi_image_to_strings(img_list_nparray , cleanup = cleanup_scratch_flag):
    string_list=[]
    for img in img_list_nparray:
        txt = image_to_string(Image.fromarray(img/np.max(img)), cleanup = cleanup_scratch_flag)
        txt = txt.translate(None, whitespace) # remove white spaces
        string_list.append(txt)
        
    return string_list
    
def call_tesseract(input_filename, output_filename):
    """Calls external tesseract.exe on input file (restrictions on types), outputting output_filename+'txt'"""
    
    language = "-l DST"
    extra_tesseract_params = "-psm 6 nobatch survey " # nobatch digits
    myargs = ['"' + tesseract_exe_name + '"' + " " + language + " " + input_filename + " " + output_filename + " " + extra_tesseract_params]
    
    #proc = subprocess.Popen(myargs, stdin = subprocess.PIPE, stdout = subprocess.PIPE)
    #stdout, retcode = proc.communicate('dir c:\\')
    retcode = subprocess.call(myargs[0] , stdin=None, stdout=False, stderr=None, shell=False)
    #proc = subprocess.Popen(myargs)
    #retcode = proc.wait()
    if retcode!=0:
         errors.check_for_errors()

def image_to_string(im, cleanup = cleanup_scratch_flag):
	"""Converts im to file, applies tesseract, and fetches resulting text.
	If cleanup=True, delete scratch files after operation."""
	try:
		util.image_to_scratch(im, scratch_image_name)
		call_tesseract(scratch_image_name, scratch_text_name_root)
		text = util.retrieve_text(scratch_text_name_root)
	finally:
		if cleanup:
			util.perform_cleanup(scratch_image_name, scratch_text_name_root)
	return text

def image_file_to_string(filename, cleanup = cleanup_scratch_flag, graceful_errors=True):
	"""Applies tesseract to filename; or, if image is incompatible and graceful_errors=True,
	converts to compatible format and then applies tesseract.  Fetches resulting text.
	If cleanup=True, delete scratch files after operation."""
	try:
		try:
			call_tesseract(filename, scratch_text_name_root)
			text = util.retrieve_text(scratch_text_name_root)
		except errors.Tesser_General_Exception:
			if graceful_errors:
				im = Image.open(filename)
				text = image_to_string(im, cleanup)
			else:
				raise
	finally:
		if cleanup:
			util.perform_cleanup(scratch_image_name, scratch_text_name_root)
	return text
	

if __name__=='__main__':
	im = Image.open('phototest.tif')
	text = image_to_string(im)
	print text
	try:
		text = image_file_to_string('fnord.tif', graceful_errors=False)
	except errors.Tesser_General_Exception, value:
		print "fnord.tif is incompatible filetype.  Try graceful_errors=True"
		print value
	text = image_file_to_string('fnord.tif', graceful_errors=True)
	print "fnord.tif contents:", text
	text = image_file_to_string('fonts_test.png', graceful_errors=True)
	print text


