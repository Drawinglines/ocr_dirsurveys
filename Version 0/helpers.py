# -*- coding: utf-8 -*-
"""
Created on Thu Apr 07 15:07:41 2016

@author: Administrator
"""

import os , subprocess
from selenium import webdriver


#==============================================================================
MainDir      = os.getcwd()
DownloadDir  = MainDir + "\Downloads"



#==============================================================================
chromeOptions = webdriver.ChromeOptions()
prefs         = {"download.default_directory" : DownloadDir}
chromeOptions.add_experimental_option("prefs",prefs)
chromedriver = "C:\Anaconda2\selenium\webdriver\chrome\chromedriver.exe"


#==== Connect to the webpage
mydriver = webdriver.Chrome(executable_path=chromedriver, chrome_options=chromeOptions)


#======== Log in and save the coockie
mydriver.get("https://uscpetro:Rock8Pore@www.dmr.nd.gov/oilgas/feeservices/getlogs.asp")
#============================================================================== 

