# -*- coding: utf-8 -*-
"""
Created on Tue Feb 16 08:51:21 2016

@author: Administrator
"""

from collections import defaultdict
import MySQLdb
import numpy as np
from datetime import datetime
import calendar


import json

from calendar import monthrange
from time import strptime , strftime
from operator import sub


CommonDir = '../../CommonFolders'
import sys
sys.path.append(CommonDir+'/combin') 
import API_main_comb as combinator




initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')
#now      = datetime.now()
#NowDatestr  = (str(now.year)+"-"+str(now.month)+"-"+str(now.day)) # "2015-04-30"
NowDatestr = str(strftime("%Y-%m-%d"))  #replaces line 19 & 20

##############################################################################################################################################################
 
def diff_month(d1str,d2str):
    d1 = datetime.strptime(d1str , '%Y-%m-%d')
    d2 = datetime.strptime(d2str , '%Y-%m-%d')
    try:
        return (d2.year - d1.year)*12 + d2.month - d1.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The aregument was {}.".format(d2)
        return np.NaN


def add_months(sourcedatestr,months):
    sourcedate = datetime.strptime(sourcedatestr , '%Y-%m-%d')    
    
    month = [ sourcedate.month - 1 + months[i] for i in xrange(len(months)) ]
    year  = [int(sourcedate.year + x / 12 ) for x in month]
    month = [ month[i] % 12 + 1 for i in xrange(len(month)) ]
    day   = [min(sourcedate.day , calendar.monthrange(year[i],month[i])[1]) for i in xrange(len(year)) ]
    
    
    day        = [calendar.monthrange(year[i],month[i])[1] for i in range(len(year))] # get the end of month day number date
    datestring = [(str(year[i])+"-"+str(month[i])+"-"+str(day[i])) for i in range(len(year))]   
    dates      = [datetime.strptime(datestring[i] , '%Y-%m-%d') for i in range(len(datestring))]    
    return datestring , dates
    
    
def diff2Dates(din):
    return 

def mynum(func, a):
    ''' This function is only a way to get around the unexpected data.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except:
        print('Expected float, got {} instead'.format(type(a)))
        return 0

def Reza_divider(a,b):
    # devides a/b safely considering b could be 0, if b==0 then a/b=0
    with np.errstate(divide='ignore', invalid='ignore'):
            c = np.true_divide(a,b)
            c[c == np.inf] = 0
            c   = np.nan_to_num(c)
    return c
##############################################################################################################################################################
 


def establish_DBconn(schema="public_data"): 
    
    with open(CommonDir+"\DB-credits\config.json", 'r') as f:
        config = json.load(f)
    
    cnx = { 'host': config["host"],'user':config["user"],'password': config["password"]}    
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'])
    
    #cnx = { 'host': config["host"],'user':config["user"],'password': config["password"],'db': schema}    
    #conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor() 
    return conn,cursor
    


##############################################################################################################################################################
#@numba.jit
def get_oilprices(cursor , schema="public_data" , tablename="oil_price_monthly" , RefDates=None , InputOilPrice_Dict=None):
    
    if InputOilPrice_Dict is None:
        cursor.execute("SELECT * FROM "+schema+"."+tablename+";")
        row = cursor.fetchall()
        oil_prices     = [row[i][1] for i in xrange(len(row))]
        oilprice_dates = [datetime.strftime(row[i][0],'%Y-%m-%d') for i in xrange(len(row))]    # ?????
        
    else:
        oil_prices     = InputOilPrice_Dict['oilprice_value']
        oilprice_dates = InputOilPrice_Dict['oilprice_date']
    del InputOilPrice_Dict
    
    
    
    
    if RefDates is not None: # given set of dates for which oil priuce is needed
        RefDates_obj = RefDates
    else:
        RefDates_obj = oilprice_dates
        
        

    #==== get the inedex of requested dates compared to ref dates in database   
    istart = diff_month(oilprice_dates[0],RefDates_obj[0])
    Indx   = range(istart,istart+len(RefDates_obj))
                
    price_date=[]
    price_val = []
    for s in Indx:
        
        try:
            price_date.append(oilprice_dates[s])
            price_val.append(oil_prices[s])
        except:
            print "Price length error"
          


    oilprice_dict = defaultdict(list) 
    oilprice_dict['oilprice_value'] = price_val    
    oilprice_dict['oilprice_date']  = price_date

    return oilprice_dict


def get_WellData_unstr(cursor,DB_ProdTable_name,DB_MasterTable_name,db,API_prod,StartDate='1900-01-01', EndDate=NowDatestr , oilprice_dict=None):
    
    #==== get column names from table
    cursor.execute("describe %s.%s" % (db , DB_ProdTable_name))
    allowed_keys = [row[0] for row in cursor.fetchall()]
    
    
    
    #==== Getting the production data from DB ====#
    Structure = ','.join(allowed_keys) # "*"
    query = "SELECT "+Structure+" FROM "+db+"."+DB_ProdTable_name +" WHERE API_prod in ( " + ",".join(map(str,API_prod)) + " ) " #and report_date>%s and report_date<=%s order by report_date ASC;"
    cursor.execute(query)
    row   = cursor.fetchall()
    
    data = defaultdict(list)
    for (i, item) in enumerate(row):
        item = list(item)
        
        
        #===== ge the relevant dates and indices for the required set
        dates = np.array([datetime.strptime(x,'%Y-%m-%d') for x in item[2].split(',')])
        index = []
        for j,x in enumerate(dates):
            if datetime.strptime(StartDate,'%Y-%m-%d') <= x < datetime.strptime(EndDate,'%Y-%m-%d'):
                index.append(j)
        dates = dates[index]
        dates = np.array([datetime.strftime(x,'%Y-%m-%d') for x in dates])
        
        
        for j,col_name in enumerate(allowed_keys):
            
            
            if type(item[j]) == type(None) or type(item[j])==long:
                temp_prod = np.array(item[j])
            elif type(item[j])==str:
                if col_name == 'dates' :# if its the date column
                    temp_prod = dates
                elif col_name == 'pool_name':
                    temp_prod = item[j]
                else:
                    try:
                        temp_prod = np.array([mynum(float,x) for x in item[j].split(',') ])[index]
                    except: 
                        print "error"
                
                
            #
            
            data[col_name].append(temp_prod)
    
    data = DLtoLD(data)  
    #--------------------------------------- impute missing data
    if bool(data):  
        Newdata = imputeMissingData(data) # impute new data if some months are missing and add the corresponding dates         
            
    #--------------------------------------- master table read    
    
    #---------------------------------------
    # get extra data needed
    # get extra dimensions and data needed
    for lnum,data in enumerate(Newdata):   
        #==== gets the oil prices and alinges them with the provided data
        OilPrice_Dict = get_oilprices(cursor , schema="public_data" , tablename="oil_price_monthly" , RefDates=data['dates'] , InputOilPrice_Dict=oilprice_dict) # oil price monthly
        Newdata[lnum]['oilprice'] = np.array(OilPrice_Dict['oilprice_value'])
        
        
        if np.shape(data['dates'])[0] != 0:
            tempDict            = combinator.encoder(data['API_prod'],data['dates'],data['proddays'],data['oil'],data['water'],['wor','time'],['linear','linear'])
            Newdata[lnum]['wor'] = np.array(tempDict['arg1'])
            del tempDict
            
            tempDict            = combinator.encoder(data['API_prod'],data['dates'],data['proddays'],data['oil'],data['water'],['cumoil','time'],['linear','linear'])
            Newdata[lnum]['cumoil'] = np.array(tempDict['arg1'])
            del tempDict
            
            tempDict            = combinator.encoder(data['API_prod'],data['dates'],data['proddays'],data['oil'],data['water'],['cumwat','time'],['linear','linear'])
            Newdata[lnum]['cumwater'] = np.array(tempDict['arg1'])
            del tempDict  

    # gather all the results
    return Newdata
    
    


def get_TestingWellPids(fname):
    content = np.loadtxt(fname,delimiter="\n")
    content = [ int(item) for item in content]
    return content
    
    
def get_WellPids(cursor , DB_ProdTable_name="big_prod_sum2" , DB_MasterTable_name="big_master" , db="public_data" , StateAPI=33):
    """
    Gets the well ids from the table data is being retrieved. 
    Also gets the well ids from the the table where results are being written to avoid duplicates.
    """
    try:
        cursor.execute("SELECT distinct(API_prod) FROM "+db+"."+DB_ProdTable_name+" WHERE state="+str(StateAPI)+";")
    except:
        DB_ProdTable_name="big_prod_sum2"
        cursor.execute("SELECT distinct(API_prod) FROM "+db+"."+DB_ProdTable_name+" WHERE state_API="+str(StateAPI)+";")
    row   = cursor.fetchall()
        
    unique_pids=set([int(v[0]) for v in row]) 
    unique_pids = filter(lambda x: x!=None, unique_pids) # remove None elements from list
    return sorted(list(unique_pids))      
    
    
def get_WellData(cursor,DB_ProdTable_name,DB_MasterTable_name,db,API_prod,StartDate='1900-01-01',EndDate=NowDatestr):
    #==== Getting the production data from DB ====
    Structure = "API_prod, state_API, report_date, pool_code, pool_name, well_status_prod, prod_days, oil, water, gas"
    #query = "SELECT * FROM "+db+"."+DB_Table_name+";"
    query = "SELECT "+Structure+" FROM "+db+"."+DB_ProdTable_name+" WHERE API_prod in ( " + ",".join(map(str,API_prod)) + " ) and report_date>%s and report_date<=%s order by API_prod , report_date ASC;"
    cursor.execute(query,[StartDate,EndDate])
    row = cursor.fetchall()
    
      
    
    
    data = defaultdict(list)
    for (i, item) in enumerate(row):
        
        #currentDate = item[2].strftime("%Y-%m-%d")
        item = np.array(map(lambda x: np.nan if x==None else x, item))
        
           
        data['API'].append( int(float(item[0])) )
        data['state_API'].append( int(float(item[1])) )
        data['dates'].append( item[2].strftime("%Y-%m-%d") )
        data['pool_code'].append( item[3] )
        data['pool_name'].append( item[4] )
        data['wellstat'].append( item[5] )
        data['proddays'].append( item[6] )
        data['oil'].append( float(item[7]) )
        data['water'].append( float(item[8]) )
        data['gas'].append( float(item[9]) )
    
    if bool(data):  
        data = imputeMissingData(data) # impute new data if some months are missing and add the corresponding dates
    
    #==== Getting the master (static) data from DB ====
    Structure = "API_master, state_API_master, well_status, well_type, lat, longi, pool_code_master, pool_name_master, spud_date"
    query = "SELECT "+Structure+" FROM "+db+"."+DB_MasterTable_name+" WHERE API_master in ( " + ",".join(map(str,API_prod)) + " );"
    cursor.execute(query)
    row = cursor.fetchall()
    for (i, item) in enumerate(row):
        data['well_type'].append( item[3] )
        data['lat'].append( float(item[4]) )
        data['longi'].append( float(item[5]) )
        data['pool_code_master'].append( item[6] )
        data['pool_name_master'].append( item[7] )
        if bool(item[8]):
            data['spud_date'].append( item[8].strftime("%Y-%m-%d") )
        else:
            data['spud_date'].append( " " )    

        
        
        #---------------------------------------
        # get extra data needed
        # get extra dimensions and data needed
        OilPrice_Dict       = get_oilprices(cursor , schema="public_data" , tablename="oil_price_monthly" , RefDates=data['dates']) # oil price monthly
        data.update(oilprice=np.array(OilPrice_Dict['oilprice_value']))
        if np.shape(data['dates'])[0] !=0 :
            
            tempDict            = combinator.encoder(data['API'],data['dates'],data['proddays'],data['oil'],data['water'],['wor','time'],['linear','linear'])
            data.update(wor=np.array(tempDict['arg1']))
            del tempDict
            
            tempDict            = combinator.encoder(data['API'],data['dates'],data['proddays'],data['oil'],data['water'],['cumoil','time'],['linear','linear'])
            data.update(cumoil=np.array(tempDict['arg1']))
            del tempDict
            
            tempDict            = combinator.encoder(data['API'],data['dates'],data['proddays'],data['oil'],data['water'],['cumwat','time'],['linear','linear'])
            data.update(cumwater=np.array(tempDict['arg1']))
            del tempDict  
    return data
    
##############################################################################################################################################################
def Preprocessing_DataDict(InData_Dict , Time_Trsh , Diff_days):
    
    #Check if it is a valid producer for forecasting
    accptflag,lastProdIndx,firstProdIndx = Check_Valid_Producer( InData_Dict , Time_Trsh , Diff_days ) 
    
    if accptflag!=0: # accepted for analysis
    
        # cut the production data after the last producing date from provided data set
        OUtData_Dict,lastProdIndx,firstProdIndx = Trim_Dictionary(InData_Dict , lastProdIndx, firstProdIndx)
        
        # transform to daily and make sure common zeros are preserved
        OUtData_Dict,dataLength  = TransformData(OUtData_Dict) # 0 consistant and scales
    
        
        
        #-------------------------------------------------
        OUtData_Dict['StartDate']      = OUtData_Dict['dates'][0]
        OUtData_Dict['EndDate']        = OUtData_Dict['dates'][-1] 
        OUtData_Dict['lastProdIndx']   = lastProdIndx
        OUtData_Dict['firstProdIndx']  = firstProdIndx
        OUtData_Dict['RunLife']        = lastProdIndx - firstProdIndx + 1
        
    else: # not accepted for analysis
        OUtData_Dict = []
        
    return OUtData_Dict , accptflag 






def Trim_Dictionary(Dict , LastProdIndx, FirstProdIndx):
    for i,key in enumerate(Dict):
        length    = len(Dict[key])
        Dict[key] = Dict[key][np.nanmax(FirstProdIndx,0):np.nanmin([LastProdIndx,length])]
    
    FirstProdIndx = 0 # update the first index of production
    LastProdIndx  = len(Dict['oil']) # updat ethe last index of production

    return Dict , LastProdIndx , FirstProdIndx
    
    
    
    
def imputeMissingData(LD_dataIn):
    ''' Accepts a list of dictionaries and analyzes them one by one '''
    
    LD_dataOut = LD_dataIn
    for lnum,dataIn in enumerate(LD_dataIn): 
    
        # determine the number of month between first and last dates given in data
        WellStartDate = dataIn['dates'][0] 
        monthnumsIndx = np.array([diff_month(WellStartDate,x) for x in dataIn['dates']])
    
        # generate the continuse month dates strings for the new data
        allindx       = range(max(monthnumsIndx)+1)
        datesstr,_    = add_months(WellStartDate,allindx)
    
        # imput zeros for missing data
        dataOut = dataIn.copy()
        for (i , key) in enumerate(dataIn):
            # Update the measured values series by imputing NaNs when data is missing
            dataOut[key] = np.empty(shape=len(datesstr) , dtype=type(dataIn[key]))#np.array(dataIn[key],dtype=type(dataIn[key][0]),ndmin=len(datesstr))
            dataOut[key].fill(0.0) # can change into zero if want 0
            if np.size(dataIn[key])==1:
                dataOut[key] = np.nan_to_num ( [dataIn[key] for i in range(len(dataOut[key]))] )
            else:
                dataOut[key][monthnumsIndx] = np.nan_to_num (dataIn[key])
        
            #==== replace nans with zeros
            #dataOut[key] = np.nan_to_num(dataOut[key]) 
        
        # get linear time index of months and the continuous dates strings
        dataOut['MonthIndx'] = np.asarray(range(max(monthnumsIndx)+1))  #range(monthnumsIndx[-1])
        dataOut['dates']     = np.asarray(datesstr)
        dataOut['API_prod']  = [np.asarray(dataOut['API_prod'][0]) for i in range(len(dataOut['API_prod']))]
        
        
        #====== get back to list of dicts
        LD_dataOut[lnum] = dataOut.copy() 
    
    return LD_dataOut


def LDtoDL (LD) :  
    result=defaultdict(list)
    for d in LD:
       for key,val in d.items():
          result[key].append(val)
    return result
def DLtoLD (DL) :
    # converts dictionary of lists to list of dicts
   if not DL :
      return []
   #reserve as much *distinct* dicts as the longest sequence
   result = [{} for i in range(max (map (len, DL.values())))]
   #fill each dict, one key at a time
   for k, seq in DL.items() :
      for oneDict, oneValue in zip(result, seq) :
          oneDict[k] = oneValue
   return result
   
##############################################################################################################################################################
 
def ScaleConvFun(inputdata):
    # this function trensforms 'overt' or back transforms 'revert' the given data into and from log space
    a = inputdata # no transformation
    b = inputdata # no back transform
    #a = np.exp(inputdata)-1 # log back transform
    #b = np.log(inputdata+1) # log transformation
    return {
        'backtrans': a,
        'transform': b,
    }
    
##############################################################################################################################################################
     
    
def TransformData(DataIn):
    # this function forward transforms the oil and water and prepares them for analysis
    # it also does daily rate conversion
    length = len(DataIn['oil'])
    for i in range(length):
    	
        # zero prods
        zero_flag = np.isnan(DataIn['proddays'][i]) or np.isnan(DataIn['oil'][i]) or (DataIn['oil'][i]==0) or (DataIn['proddays'][i]==0)
        if zero_flag==True:
            DataIn['proddays'][i] = 1.0
        
        
        #DataIn['oil'][i]   = Reza_divider(DataIn['oil'][i]   , DataIn['proddays'][i])
        #DataIn['water'][i] = Reza_divider(DataIn['water'][i] , DataIn['proddays'][i] )
        #DataIn['gas'][i]   = Reza_divider(DataIn['gas'][i]   , DataIn['proddays'][i] )
        
        DataIn['oil'][i]   /= DataIn['proddays'][i]                 # This makes oil per day (oil_pd)
        DataIn['water'][i] /= DataIn['proddays'][i]                 # This makes water per day (water_pd)
        #DataIn['gas'][i]   /= DataIn['proddays'][i]                      # This makes gas per day (gas_pd)
        
        
        
        
        if zero_flag==True:
            DataIn['oil'][i]      = 0.0
            DataIn['water'][i]    = 0.0
            #DataIn['gas'][i]      = 0.0
            DataIn['proddays'][i] = 0.0
    	
        # Take log of oil
        DataIn['oil'][i] = ScaleConvFun(DataIn['oil'][i])['transform']
        #DataIn['oil'][i][np.isinf(DataIn['oil'][i])] = 0
        #DataIn['oil'][i][np.isnan(DataIn['oil'][i])] = 0
        
        DataIn['water'][i] = ScaleConvFun(DataIn['water'][i])['transform']
        #DataIn['water'][i][np.isnan(DataIn['water'][i])] = 0
        #DataIn['water'][i][np.isinf(DataIn['water'][i])] = 0
        
        #DataIn['gas'][i] = ScaleConvFun(DataIn['gas'][i])['transform']


    DataOut = DataIn.copy()
    return DataOut,length





def BackTransformData(DataIn):
    # this function back transforms the given data from daily format to monthly format
    length = len(DataIn['oil'])
    for i in range(length):
    	
        # zero prods
        zero_flag = np.isnan(DataIn['proddays'][i]) or np.isnan(DataIn['oil'][i]) or (DataIn['oil'][i]==0) or (DataIn['proddays'][i]==0)
        if zero_flag==True:
            DataIn['proddays'][i] = 0
        
        
        
       # DataIn['oil_Fore'][i] *= DataIn['proddays'][i]            # This makes oil per day (oil_pd)
       # DataIn['oil'][i]   *= DataIn['proddays'][i]                 # This makes oil per day (oil_pd)
       # DataIn['water'][i] *= DataIn['proddays'][i]                 # This makes water per day (water_pd)
       # DataIn['gas'][i]   *= DataIn['proddays'][i]                 # This makes gas per day (water_pd)
        
        
        
        
       # if zero_flag==True:
       #     DataIn['oil_Fore'][i] = 0
       #     DataIn['oil'][i]    = 0
       #     DataIn['water'][i]  = 0
       #     DataIn['gas'][i]    = 0
       #     DataIn['proddays'][i] = 0
    	
        # Take log of oil
        DataIn['oil_Fore'][i] = ScaleConvFun(DataIn['oil_Fore'][i])['backtrans']
     
        DataIn['oil'][i] = ScaleConvFun(DataIn['oil'][i])['backtrans']

        
        DataIn['water'][i] = ScaleConvFun(DataIn['water'][i])['backtrans']

        
        DataIn['gas'][i] = ScaleConvFun(DataIn['gas'][i])['backtrans']



    DataOut = DataIn.copy()
    return (DataOut,length)
    
    
##############################################################################################################################################################
      
    
    
def Check_Valid_Producer(data , Time_Trsh=2 , Diff_days=2 ):    
    # this function checks whether the well is a valid producer for forecasting and gives
    # the index of when the well gone offline by checking the catastrophic failure criterias
    flag        =1;
    lastProdindx=0
    
    # Diff_days=3 # total days its okey for the well not to be producing in a month
      
    if bool(data)==False:
        flag=0
        return flag,lastProdindx,0
        
    """%------------------------------------------------------------
    % Check the days down for given data
    %------------------------------------------------------------"""
    oil      = np.array(data['oil'],float)
    water    = np.array(data['water'],float)
    Welldates= data['dates']
    proddays = np.array(data['proddays'],float)
    DaysDown,DaysInMonth = DownTime(Welldates,proddays)    
        
    """%------------------------------------------------------------
    % get the index of last producing day: the logic is
    % that the last fully producing month is just before the CF
    %------------------------------------------------------------"""
    
    if np.sum(np.abs(oil))==0: # never produced
        flag = 0
        return flag,lastProdindx,0
    if np.sum(np.abs(oil))==0 and np.sum(np.abs(water))!=0 : # its and injector
        flag = 0
        return flag,lastProdindx,0
            
    
    

    indx=[y for y in xrange(len(proddays)) if DaysInMonth[y]-proddays[y]<=Diff_days]
    
    indx.sort() #array has to be sorted in descending order. This will sort in ascending the mirror command next arranges in desc
    indx=indx[::-1]
    
    if len(indx)==0 :#and indx.size==0:    # OR if indx is a list => if not indx:
        if sum(proddays)!=0: 
            """zero production but non zero proddays -> strange well"""
            print ("Never produced a full month !!")        
        else:
            """no production what so ever"""
            print ("This well is an injector !!!")
        flag=0
        return flag,lastProdindx,0
    else: 
        """its a producer
        %----------------------------------------------------
        % get the temporary active situations after CF
        %----------------------------------------------------"""
        indxset1=[x for x in xrange(len(oil)) if oil[x]!=0]
        indxset1.sort()
        indxset1=indxset1[::-1]
        """index of start of CF with proddays<=Daysinmonth"""
        indx1=0
        if bool(indxset1):
            indx1=indxset1[0] 
        
        
        """indexes where qoil is zero"""
        indxtemp2=[z for z in xrange(len(oil)) if oil[z]==0]
        indxtemp2.sort()
        indxtemp2=indxtemp2[::-1]
        
        """----- need to find the first zero before the CF time happening"""
        indxset2=[indxtemp2[a] for a in xrange(len(indxtemp2)) if indxtemp2[a]>indx1]
        if len(indxset2)<1:
            indx2=0
        else:
            indx2=max(indxset2)
        
        #find index of last 0 before indxset2 
        temp=indxset2[0:indx2+1] #.tolist()
        temp=temp[::-1]
        if len(temp)<1:
            """the zero prod day before indx2"""
            indx3=0        
        else:
            indx3= min(temp) #temp.index(0)
        
        """if the production time between the CF and the last 0 point is smaller than 4 month
        % and the produced volume is less than 2500 bbl"""
        
        if((indx1-indx2)<=4) and (sum(oil[indx2:indx1]))<=2500  and ((indx2 - indx3) >= 4) and indx3==None: #and (~len(indx3)==0)
            indx=[indx[b] for b in xrange(len(indx)) if indx[b]<indx3]
            """update the indx since the temporary situation of active has happened"""
        
        """End of Temp situation"""
        a=indx[0]
        lastProdindx=a-int(proddays[a]<DaysInMonth[a]);
        
        # get the first nonzero production day
        # get the first non-zero production 
        try:
            firstProdindx = np.where(oil>0.0)[0][0]
        except:
            firstProdindx = 0
        
    """%-----------------------------------------------
        % check on whether changed to injector too soon
        %-----------------------------------------------"""
    
    if lastProdindx-firstProdindx<Time_Trsh: 
        """if well has been a producer less this number of months only"""
        print ("Changed to injector Too Soon !!")
        flag=0
    
    return flag,lastProdindx,firstProdindx
    
    
    
'''-------------------------------------------------
This function calculates the number of days well has been down in each month
---------------------------------------------------'''    
def DownTime(ProdEndDates,ProdDays):
     
    A=[]
    DaysInMonth=[]
    
    for i in xrange(len(ProdEndDates)):
        #array of Dates extract Month and Year from the string and find the number of days in that month
    
        x=strptime(ProdEndDates[i],"%Y-%m-%d")
        A.append(x)
        DaysInMonth.append(monthrange(x.tm_year,x.tm_mon)[1])          
    
    #DaysDown= (a-b for a,b zip(DaysInMonth,ProdDays))
    DaysDown=map(sub, DaysInMonth,ProdDays)
    return DaysDown , DaysInMonth
    
##############################################################################################################################################################
 
