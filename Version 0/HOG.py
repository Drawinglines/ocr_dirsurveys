# -*- coding: utf-8 -*-
"""
Created on Tue Apr 26 11:03:52 2016

@author: Administrator
"""

import matplotlib.pyplot as plt
plt.close("all")

from skimage.feature import hog
from skimage import data, color, exposure
import cv2

ImageDir = "C:/Users/Administrator/Desktop/Reza Projects/OCR Well Prod/Proddata_digitization/tests1"

Im_orig = cv2.imread(ImageDir+'/survey_1.tif',1)
image = color.rgb2gray(Im_orig) #data.astronaut())

fd, hog_image = hog(image, orientations=8, pixels_per_cell=(20, 20),
                    cells_per_block=(1, 1), visualise=True)

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8, 4), sharex=True, sharey=True)

ax1.axis('off')
ax1.imshow(image, cmap=plt.cm.gray)
ax1.set_title('Input image')
ax1.set_adjustable('box-forced')

# Rescale histogram for better display
#hog_image_rescaled = hog_image
hog_image_rescaled = exposure.rescale_intensity(hog_image, in_range=(0, 0.02))

ax2.axis('off')
ax2.imshow(hog_image_rescaled, cmap=plt.cm.gray)
ax2.set_title('Histogram of Oriented Gradients')
ax1.set_adjustable('box-forced')
plt.show()