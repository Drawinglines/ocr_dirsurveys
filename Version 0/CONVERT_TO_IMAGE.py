# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 10:03:00 2016

@author: Administrator
"""

ImageDir = "C:/Users/Administrator/Desktop/Reza Projects/OCR Well Prod/Proddata_digitization/tests1"

'''
from wand.image import Image

# Converting first page into JPG
with Image(filename=ImageDir+"/test1.pdf[43]") as img:
     img.save(filename=ImageDir+"/temp.tif")
     
# Resizing this image
with Image(filename="/temp.jpg") as img:
     img.resize(200, 150)
     img.save(filename="/thumbnail_resize.jpg")
     '''
     
     
from wand.image import Image
from wand.color import Color
import os, os.path, sys

def pdf2tif(source_file, target_file, dest_width, dest_height):
    RESOLUTION    = 600
    ret = True
    try:
        with Image(filename=source_file, resolution=(RESOLUTION,RESOLUTION)) as img:
            img.background_color = Color('white')
            img_width = img.width
            ratio     = dest_width / img_width
            #img.resize(dest_width, int(ratio * img.height))
            img.format = 'tif'
            img.alpha_channel=False
            img.save(filename = target_file)
    except Exception as e:
        ret = False

    return ret

if __name__ == "__main__":
    source_file = ImageDir+"/test1.pdf[44]"
    target_file = ImageDir+"/survey_3.tif"

    ret = pdf2tif(source_file, target_file, 1895, 1080)