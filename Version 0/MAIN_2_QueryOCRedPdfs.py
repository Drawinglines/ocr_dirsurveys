# -*- coding: utf-8 -*-
"""
Created on Sun Apr 10 08:08:17 2016

@author: Reza
"""
#====== this function is a wrapper around fnPDF_FindText but can handle ands and ors in strings
def fnPDF_FindExpression(xFile,xExpression):
    
    
    '''currently works only with ands and or and not a combination of both
    or = ',' in the expression
    and = ' ' in the expression
    '''
    
    Page_list = set([])
    orlist_expression = xExpression.split(',')
    for exp in orlist_expression:
        list_expression = exp.split('+')
        
        
        for i,content in enumerate(list_expression):
            temp = set(fnPDF_FindText(xFile, content))
            
            if i==0:
                andPage_list = temp
            else:
                andPage_list = andPage_list & temp
        
        Page_list = Page_list | andPage_list
    

    
    return list(Page_list)
    
    





 
#====== finction that finds the page containing the search string
def fnPDF_FindText(xFile, xString):
    # xfile : the PDF file in which to look
    # xString : the string to look for
    import pyPdf, re
    PagesFound = []
    pdfDoc = pyPdf.PdfFileReader(file(xFile, "rb"))
    for i in range(0, pdfDoc.getNumPages()):
        content = ""
        content += pdfDoc.getPage(i).extractText() + "\n"
        content1 = content.encode('ascii', 'ignore').lower()
        ResSearch = re.search(xString, content1)
        if ResSearch is not None:
           PagesFound.append(i)
           #break
    return PagesFound
    
    
#===== This function extracts the pages of interest
def fnPDF_ExtractPages(xFileNameOriginal, xFileNameOutput, xPageNumbers):
    from pyPdf import PdfFileReader, PdfFileWriter
    output = PdfFileWriter()
    pdfOne = PdfFileReader(file(xFileNameOriginal, "rb"))
    for i in xPageNumbers:
          output.addPage(pdfOne.getPage(i))
          outputStream = file(xFileNameOutput, "wb")
          output.write(outputStream)
          outputStream.close()
          

#==== This function finds the files with specific extensions in a directory
def find_ext(dr, ext):
    from os import path
    from glob import glob  
    return glob(path.join(dr,"*.{}".format(ext)))
    
    
    
    
    
    
#===== main ===================================================================

#==== find list of pdfs downloaded for analysis
Well_Reports = find_ext('Downloads','pdf')
for i,report_pdf in enumerate(Well_Reports):
    
    #==== extract directional survey pages
    xFileNameOriginal = report_pdf
    
    contents = ['perforations+well+report' , 'survey , measured_depth , closure' , 'abandon' ,'Operation' , 'core+log']
    #contents = ['perforation' , 'survey,survey data' , 'abandon' ,'Operation' , 'core']
    prefix   = ['perforations' , 'survey' ,'abandonment' , 'operations' , 'coreReport']
    
    #contents = ['survey,directional']
    #contents = ['survey,measured+depth,closure']
    #prefix = contents
    for j,content in enumerate(contents):

        #----- set up the name for output file
        indxtmp = xFileNameOriginal.find('DATA')
        xFileNameOutput   = 'Results' + "\\" + xFileNameOriginal[:indxtmp] + prefix[j] + '_' + xFileNameOriginal[indxtmp:]
        xFileNameOutput   = xFileNameOutput.replace('Downloads\\','',1)
    
    
    
        #------ get page numbers for given string
        pages = fnPDF_FindExpression(xFileNameOriginal,content)
        #print pages
        #----- extract pages and  save them in results folder
        fnPDF_ExtractPages(xFileNameOriginal, xFileNameOutput, pages)
        
    print 'Finished file NO:' + str(1+i) + ' out of (' + str(len(Well_Reports)) +')'
        



