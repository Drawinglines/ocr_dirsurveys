# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 15:13:06 2016

@author: Administrator
"""


import numpy as np
from matplotlib import pyplot as plt
plt.close("all")

import Reza_OCR_Helpers as OCR_hp


# <codecell>
# =============================================================================
# ==== Initialization
ImageDir     = "C:/Users/Administrator/Desktop/Reza Projects/OCR Well Prod/Proddata_digitization/tests1"
list_of_pdfs = ["test1.pdf[42]"]


#%%
def Digitize_Directional_Surveys(args):
    
    # ===== Extract the arguments
    pdf_page_name = args[0]
    source_file   = ImageDir+"/"+pdf_page_name
    
    # ====== convert page to image
    orig_color_npArray = np.asarray(OCR_hp.pdf2tif(source_file))
    
    # ===== extract horizontal and vertical lines
    lines, lines_vert , lines_horiz = OCR_hp.Extract_GridLines(orig_color_npArray)
    
    # ===== OCR each cell and get the full csv table
    OCR_table = OCR_hp.cell_OCR( orig_color_npArray , lines_vert, lines_horiz)
    
    # ===== 




# <codecell>
# =============================================================================
# ================ Get lines of the
# =============================================================================


