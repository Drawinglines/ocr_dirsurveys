# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 15:19:53 2016

@author: Administrator
"""

# <codecell>
# =============================================================================
# ============== Convert pdf pages to images for processing
# =============================================================================
from wand.image import Image
from wand.color import Color

def pdf2tif(source_file, target_file="pdf.tif", dest_width=1895, dest_height=1080):
    RESOLUTION = 600
    ret = True
    try:
        with Image(filename=source_file, resolution=(RESOLUTION,RESOLUTION)) as img:
            img.background_color = Color('white')
            #img_width = img.width
            #ratio     = dest_width / img_width
            #img.resize(dest_width, int(ratio * img.height))
            img.format = 'tif'
            img.alpha_channel=False
            #img.save(filename = target_file)
    except Exception as e:
        ret = False

    return ret , img
'''    
if __name__ == "__main__":
    source_file = ImageDir+"/test1.pdf[44]"
    target_file = ImageDir+"/survey_3.tif"

    ret = pdf2tif(source_file, target_file, 1895, 1080)
'''




# <codecell>
import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.filters import  threshold_adaptive
from skimage.filters.rank import median
from skimage.morphology import disk
# =============================================================================
# ============== Extracting Table GridLines
# =============================================================================
#%% extract the gridlines function
def Extract_GridLines(color_npArray):
    
    #%% convert image to gray
    Im_gray = cv2.cvtColor(color_npArray, cv2.COLOR_BGR2GRAY)
    #OCR_hp.showImage(Im_gray,'gray')




    #%% threshold image to darken lines
    #Im_gray = OCR_hp.Hist_Stretching(Im_gray)
    Im_bin       = np.uint8(threshold_adaptive(Im_gray, block_size = 45, offset=15))
    Im_bin_txt   = median(Im_bin, disk(8))
    Im_bin_table = Im_bin-Im_bin_txt
    #OCR_hp.showImage(Im_bin_table,'gray')

    
    #%% filter image to smooth
    #Im_blurred = Im_gray.copy()
    #Im_blurred = cv2.GaussianBlur(Im_gray, (3, 3), 0)
    #blurred   = cv2.medianBlur(img,5)
    #showImage(Im_blurred,'gray')


    #%% Edge detection
    Im_autocanny=Im_bin_table
    #Im_autocanny = OCR_hp.auto_canny(Im_gray)
    #OCR_hp.showImage(Im_autocanny,'gray')

    #%% Hough transform for line extraction
    lines_vert  = Reza_houghLines(Im_autocanny,rho_res=1,theta_res=np.pi,hough_trsh=450,clean_trsh=30,linetype='vert')
    lines_horiz = Reza_houghLines(Im_autocanny,rho_res=1,theta_res=np.pi/2.0,hough_trsh=400,clean_trsh=30,linetype='horiz')
    #lines = lines_horiz
    lines = np.concatenate((lines_vert , lines_horiz),axis=0)
    
    #%% plot the lines on image overlayed           
    '''
    m_hough = color_npArray.copy()
    for line in lines:
        rho,theta  = line
        #print rho 
        #print theta
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a*rho
        y0 = b*rho
        x1 = int(x0 + 10000*(-b))
        y1 = int(y0 + 10000*(a))
        x2 = int(x0 - 10000*(-b))
        y2 = int(y0 - 10000*(a))
    
        cv2.line(Im_hough,(x1,y1),(x2,y2),(0,0,255),5)

    #cv2.imwrite(ImageDir+'/houghlines3.jpg',Im_hough)
    OCR_hp.showImage(Im_hough,'color')
    '''
    
    
    return lines , lines_vert , lines_horiz
    
    

# <codecell>
import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
from skimage import exposure
import scipy.cluster.hierarchy as hcluster
import Reza_pytesser as pytesser
import os
from skimage.morphology import disk
from skimage.filters.rank import median
# =============================================================================
# ============== Image Processing Modules Used on Other modules
# =============================================================================
#%% Simple enhancement operations
def Hist_Stretching(Im_gray):
    # Contrast stretching
    p2, p98 = np.percentile(Im_gray, (10, 90))
    img_rescale = exposure.rescale_intensity(Im_gray, in_range=(p2, p98))
    return img_rescale

def showImage(inputImage,colormap):

    if colormap=="color":
        plt.figure().add_subplot(111).imshow(cv2.cvtColor(inputImage, cv2.COLOR_BGR2RGB))
    else:
        plt.figure().add_subplot(111).imshow(inputImage,colormap)


#%% Feature extraction operations
def auto_canny(image, sigma=0.33):
	# compute the median of the single channel pixel intensities
	v = np.median(image)
 
	# apply automatic Canny edge detection using the computed median
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper,apertureSize = 3 , L2gradient =True)
 
	# return the edged image
	return edged


#%%
def Reza_houghLines(canny_Img,rho_res=1,theta_res=np.pi/2.0,hough_trsh=450,clean_trsh=40.0,linetype='vert'):

    #==== vertical lines
    lines = cv2.HoughLines(canny_Img,rho_res,theta_res,threshold=hough_trsh) # threshold is the minimum vote the point should get to be a line
    lines = np.vstack(lines.tolist()) # only the rho matters all the lines are vertical        
        
    if linetype=='vert':
       #== remove vertical lines
       unnec_line_flag = lines[:,1]==np.pi/2.0
    elif linetype=='horiz':
        #== remove vertical lines
        unnec_line_flag = lines[:,1]==0.0
    else:
        print "not suported yet"
    
    rmv_indx = [i for i in range(len(unnec_line_flag)) if unnec_line_flag[i]==True]
    lines    = np.delete(lines,rmv_indx,0)  
    
    
    #norm_array = np.linalg.norm(lines,2,0,keepdims=True)
    norm_array = np.std(lines,0,keepdims=True)
    mean_array = np.mean(lines,0,keepdims=True)
    
    cont_indx = (norm_array==0)[0]
    norm_array[0][np.where(cont_indx)]=1.0
    mean_array[0][np.where(cont_indx)]=0.0
    
    # rescale to normalize
    lines = ((lines-mean_array[:,None]) / norm_array[:,None])[0,:,:] 
    # clean up
    lines = cleanHoughLines(lines,thresh=clean_trsh)  
    # scale back 
    lines = ((lines * norm_array[:,None]) + mean_array[:,None])[0,:,:]

    return lines
    
    
def cleanHoughLines(X,thresh=40.0):
    
    # clustering
    cluster_lables = hcluster.fclusterdata(X, thresh, criterion="maxclust",method="average") # distance maxclust #centroid complete
    UniqueClusters=np.unique(cluster_lables)
    
    Clustermeans_X =[]
    for i in UniqueClusters:
        #Clustermeans_X.append(np.percentile(X[cluster_lables==i],0.8,0)) # could use mean med or any other statistics
        Clustermeans_X.append(np.min(X[cluster_lables==i],0)) # could use mean med or any other statistics

    return Clustermeans_X
    
# <codecell>   
# =============================================================================
# ============== Optical Character Recognition OCR Tools
# =============================================================================    
#%% OCR a single cell provided by image and horizontal and vertical lines
def cell_OCR(Im_orig , vertical, horizontal):
    
    
    # sort the lines
    vertical   = np.sort(vertical,0)
    horizontal = np.sort(horizontal,0)
    # go through each horizontal line (aka row)
    
    table=[]
    for i, h in enumerate(horizontal):
        if i < len(horizontal)-1:
            row = []
            for j, v in enumerate(vertical):
                if i < len(horizontal)-1 and j < len(vertical)-1:
                    # every cell before last cell
                    # get width & height
                    width  = horizontal[i+1][0] - h[0]
                    height = vertical[j+1][0] - v[0]

                else:
                    # last cell, width = cell start to end of image
                    # get width & height
                    width  = 1
                    height = 1
                    #width  = np.shape(Im_orig)[0] - h[0]
                    #height = np.shape(Im_orig)[1] - v[0]
                
                if width*height<=8:
                    continue
                
                    

                # get roi (region of interest) to find an x
                roi = Im_orig[int(h[0]):int(h[0]+width), int(v[0]):int(v[0]+height)]

                
                
                if i<9 or i>=10:
                    continue
                
                '''
                if i==10 and j==11:
                    print "hi"
                else:
                    continue
                '''
                
                txtocr=[]
                filterLevels = [8]
                for diskdim in filterLevels:
                
                    Proc_img_list = PreProcess_ImageCell_For_OCR(roi,diskdim)
                    txt_list      = pytesser.multi_image_to_strings(Proc_img_list , cleanup = True)
                    
                    #==== get the txt with maximum likelihood
                    conts = [txt_list.count(s) for s in txt_list]
                    indx  = np.argmax(conts)
                    thresh= Proc_img_list[indx]
                    txt   = txt_list[indx]
                    
                    #==== OCR with Tesseract
                    img_ocr    = thresh.copy()
                    annot_img  = img_ocr.copy()
                    txtocr.append(txt)
                
                
                
                conts = [txtocr.count(s) for s in txtocr]
                indx  = np.argmax(conts)
                txt   = txtocr[indx]
                
                
                '''
                #===== put the text on image
                cv2.putText(annot_img,txt, (60,60),cv2.FONT_HERSHEY_SIMPLEX, 1,(0,0,255),2)
                    
                dir = 'table%s' % (1)
                if not os.path.exists(dir):
                       os.makedirs(dir)
                fn1 = '%s/roi_r%s-c%s.png' % (dir, i, j)
                
                dir = 'table%s' % (2)
                if not os.path.exists(dir):
                       os.makedirs(dir)
                fn2 = '%s/roi_r%s-c%s.png' % (dir, i, j)
                
                #if txt!='':
                cv2.imwrite(fn1, annot_img)
                cv2.imwrite(fn2, img_ocr )
                '''
          
                #===== Put the retrieved txt into a list of lists to be written as csv file
                #===== each row is a list in the list of lists
                row.append(txt)
              
            table.append(row)
                        
    return table
                    
                
#%% Tools for trimming and enhancing for ocr module               
def trim(in_npArray):
    from PIL import Image, ImageChops
    im = Image.fromarray(in_npArray)
    bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        return np.uint8(np.asarray(im.crop(bbox)))
    else:
        return in_npArray
        
def trim_borders(in_Array):
    width  = np.shape(in_Array)[0]
    height = np.shape(in_Array)[1]
    crw = 5
    crh = 5
    out_Array = in_Array[crw:width-crw , crh:height-crh]
    return out_Array
         
def PreProcess_ImageCell_For_OCR(roi,diskdim):
     out_Grey_ImageNpArray = roi.copy()
    
     
     roi_gry     = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
     #thresh = roi_gry.copy()
     thresh1 = roi_gry
     thresh1     = Hist_Stretching(roi_gry)
     
     #thresh      = imclearborder(thresh1, diskdim)
     thresh      = median(thresh1, disk(diskdim))
     ret, thresh = cv2.threshold(thresh, 30 , 255, cv2.THRESH_BINARY)  
     #thresh      = np.uint8(255.0*(threshold_adaptive(thresh, block_size = 45, method ='mean' , offset=20)))
     
     
     
     Im_autocanny = auto_canny(thresh)
     kernel       = cv2.getStructuringElement(cv2.MORPH_CROSS,(15,15))
     dilated      = cv2.dilate(Im_autocanny,kernel,iterations = 15) # dilate
     im2, contours, hierarchy = cv2.findContours(dilated,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE ) 
     
     cnt = contours[0]
     x,y,w,h = cv2.boundingRect(cnt)
     thresh = [thresh[y:y+h,x:x+w]]
     
     '''
     # for each contour found, draw a rectangle around it on original image
     img_out=[]
     for contour in contours:
            # get rectangle bounding contour
            [x,y,w,h] = cv2.boundingRect(contour)
    
            # discard areas that are too small
            if h<8 or w<8:
                continue
        
            # draw rectangle around contour on original image
            #cv2.rectangle(thresh,(x,y),(x+w,y+h),(255,0,255),2)
            img_out.append(thresh[y:y+h,x:x+w])
            
       
     
     '''
     
     #thresh      = [trim(thresh[0])]

     
     out_Grey_ImageNpArray = thresh
     #del thresh , thresh1 , roi_gry
     return out_Grey_ImageNpArray
                   
                   
                   
#### imclearborder definition

def imclearborder(imgBW, radius):
    
    # Given a black and white image, first find all of its contours
    imgBWcopy = imgBW.copy()
    _,contours,hierarchy = cv2.findContours(imgBWcopy.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    # Get dimensions of image
    imgRows = imgBW.shape[0]
    imgCols = imgBW.shape[1]    

    contourList = [] # ID list of contours that touch the border

    # For each contour...
    for idx in np.arange(len(contours)):
        # Get the i'th contour
        cnt = contours[idx]

        # Look at each point in the contour
        for pt in cnt:
            rowCnt = pt[0][1]
            colCnt = pt[0][0]

            # If this is within the radius of the border
            # this contour goes bye bye!
            check1 = (rowCnt >= 0 and rowCnt < radius) or (rowCnt >= imgRows-1-radius and rowCnt < imgRows)
            check2 = (colCnt >= 0 and colCnt < radius) or (colCnt >= imgCols-1-radius and colCnt < imgCols)

            if check1 or check2:
                contourList.append(idx)
                break

    for idx in contourList:
        cv2.drawContours(imgBWcopy, contours, idx, (0,0,0), -1)

    return imgBWcopy
