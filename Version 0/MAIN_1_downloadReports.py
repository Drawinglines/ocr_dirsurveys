# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 09:02:34 2016

@author: Reza
"""

import reza_helpers as reza_hp
from selenium import webdriver
import os 
import time

#==============================================================================
# ===== Get links from data base table for files on s3 bucket
query = "SELECT links FROM new_data.filenames_laall_logs WHERE field_name='Bandini' AND Name_file LIKE '%DATA%'"

conn,cursor = reza_hp.establish_DBconn()
cursor.execute(query)
s3bucket_links = cursor.fetchall()






#==============================================================================
DownloadDir  = "C:\Users\Administrator\Desktop\Reza Projects\OCR Well Prod\Proddata_digitization\Downloads"
MainDir      = os.getcwd()


#==============================================================================
chromeOptions = webdriver.ChromeOptions()
prefs         = {"plugins.plugins_list": [{"enabled":False,"name":"Chrome PDF Viewer"}] , "download.default_directory" : DownloadDir}
chromeOptions.add_experimental_option("prefs",prefs)
chromedriver = "C:\Anaconda2\selenium\webdriver\chrome\chromedriver.exe"

#==== Connect to the webpage
mydriver = webdriver.Chrome(executable_path=chromedriver, chrome_options=chromeOptions)

#======== download the pdf file
for i,link in enumerate(s3bucket_links):
    mydriver.get(link[0])
    time.sleep(2)

        
#============================================================================== 
# ======== Close the driver and browser    

mydriver.quit()        
 




