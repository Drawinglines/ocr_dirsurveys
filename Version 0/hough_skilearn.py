# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 14:26:00 2016

@author: Administrator
"""

# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 10:31:52 2016

@author: Administrator
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.filters import threshold_otsu, threshold_adaptive

import ImageOCR_Reza_helpers as OCR_hp
plt.close("all")

ImageDir = "C:/Users/Administrator/Desktop/Reza Projects/OCR Well Prod/Proddata_digitization/tests1"



    
#==============================================================================
#== load the original image
Im_orig = cv2.imread(ImageDir+'/survey_2.tif',1)
OCR_hp.showImage(Im_orig,'color')


#== get the gray scale image
Im_gray = cv2.cvtColor(Im_orig, cv2.COLOR_BGR2GRAY)
#Im_gray    = cv2.imread(ImageDir+'/survey_2.tif',cv2.IMREAD_GRAYSCALE  ) # cv2.IMREAD_GRAYSCALE
OCR_hp.showImage(Im_gray,'gray')




#== threshold image to darken lines
#ret,Im_gray = cv2.threshold(Im_gray,190,255,cv2.THRESH_TOZERO)
#OCR_hp.showImage(Im_gray,'gray')


#== filter image to smooth
#Im_blurred = Im_gray.copy()
#Im_blurred = cv2.GaussianBlur(Im_gray, (3, 3), 0)
#blurred   = cv2.medianBlur(img,5)
#showImage(Im_blurred,'gray')


#== Edge detection
block_size = 45
Im_gray      = np.uint8(threshold_adaptive(Im_gray, block_size, offset=10))
Im_autocanny = OCR_hp.auto_canny(Im_gray)
OCR_hp.showImage(Im_autocanny,'gray')


'''
newImg,rows = OCR_hp.hough_transform_p(Im_autocanny,Im_orig)
OCR_hp.showImage(newImg,'color')
'''


#== hough transform to extract the lines
lines_vert  = cv2.HoughLines(Im_autocanny,1,np.pi,threshold=500) # threshold is the minimum vote the point should get to be a line

#lines_horiz = cv2.HoughLines(Im_autocanny,1,np.pi/2,threshold=450)

#lines_vert = OCR_hp.cleanHoughLines(lines_vert,distance=3)
#linestemp = OCR_hp.cleanHoughLines(lines_vert,distance=4)
#lines_vert = OCR_hp.cleanHoughLines(lines_vert,distance=100000)

#lines = lines_vert + lines_horiz  
            
Im_hough = Im_orig.copy()

for line in lines_vert:
    rho,theta  = line[0]
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a*rho
    y0 = b*rho
    x1 = int(x0 + 10000*(-b))
    y1 = int(y0 + 10000*(a))
    x2 = int(x0 - 10000*(-b))
    y2 = int(y0 - 10000*(a))

    cv2.line(Im_hough,(x1,y1),(x2,y2),(0,0,255),2)

#cv2.imwrite(ImageDir+'/houghlines3.jpg',Im_hough)
OCR_hp.showImage(Im_hough,'color')



'''
minLineLength = 50
maxLineGap    = 20
lines = cv2.HoughLinesP(Im_autocanny,1,np.pi,100,minLineLength,maxLineGap)
#lines = cleanHoughLines(lines,distance=10)

Im_hough = Im_orig.copy()

for line in lines:
    x1,y1,x2,y2 = line[0]
    cv2.line(Im_hough,(x1,y1),(x2,y2),(0,255,0),2)

#cv2.imwrite(ImageDir+'/houghlines3.jpg',Im_hough)
showImage(Im_hough,'color')

#Threshold = cv2.adaptiveThreshold(imgray,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,11,2)
#showImage(Threshold,'gray')

#plt.imshow(img,'gray')
'''