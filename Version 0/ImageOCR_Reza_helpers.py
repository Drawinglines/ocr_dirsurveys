# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 11:23:38 2016

@author: Administrator
"""

import cv2
import numpy as np
from matplotlib import pyplot as plt
from PIL import Image
from skimage import exposure
import scipy.cluster.hierarchy as hcluster
import Reza_pytesser as pytesser
import os
from skimage.morphology import disk
from skimage.filters.rank import median

from skimage.filters import threshold_otsu, threshold_adaptive

import tifffile as tiff

def Hist_Stretching(Im_gray):
    # Contrast stretching
    p2, p98 = np.percentile(Im_gray, (10, 90))
    img_rescale = exposure.rescale_intensity(Im_gray, in_range=(p2, p98))
    return img_rescale

def showImage(inputImage,colormap):

    if colormap=="color":
        plt.figure().add_subplot(111).imshow(cv2.cvtColor(inputImage, cv2.COLOR_BGR2RGB))
    else:
        plt.figure().add_subplot(111).imshow(inputImage,colormap)



def auto_canny(image, sigma=0.33):
	# compute the median of the single channel pixel intensities
	v = np.median(image)
 
	# apply automatic Canny edge detection using the computed median
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper,apertureSize = 3 , L2gradient =True)
 
	# return the edged image
	return edged


def Reza_houghLines(canny_Img,rho_res=1,theta_res=np.pi/2.0,hough_trsh=450,clean_trsh=40.0,linetype='vert'):

    #==== vertical lines
    lines = cv2.HoughLines(canny_Img,rho_res,theta_res,threshold=hough_trsh) # threshold is the minimum vote the point should get to be a line
    lines = np.vstack(lines.tolist()) # only the rho matters all the lines are vertical        
        
    if linetype=='vert':
       #== remove vertical lines
       unnec_line_flag = lines[:,1]==np.pi/2.0
    elif linetype=='horiz':
        #== remove vertical lines
        unnec_line_flag = lines[:,1]==0.0
    else:
        print "not suported yet"
    
    rmv_indx = [i for i in range(len(unnec_line_flag)) if unnec_line_flag[i]==True]
    lines    = np.delete(lines,rmv_indx,0)  
    
    
    #norm_array = np.linalg.norm(lines,2,0,keepdims=True)
    norm_array = np.std(lines,0,keepdims=True)
    mean_array = np.mean(lines,0,keepdims=True)
    
    cont_indx = (norm_array==0)[0]
    norm_array[0][np.where(cont_indx)]=1.0
    mean_array[0][np.where(cont_indx)]=0.0
    
    # rescale to normalize
    lines = ((lines-mean_array[:,None]) / norm_array[:,None])[0,:,:] 
    # clean up
    lines = cleanHoughLines(lines,thresh=clean_trsh)  
    # scale back 
    lines = ((lines * norm_array[:,None]) + mean_array[:,None])[0,:,:]

    return lines
    
    
def cleanHoughLines(X,thresh=40.0):
    
    # clustering
    cluster_lables = hcluster.fclusterdata(X, thresh, criterion="maxclust",method="average") # distance maxclust #centroid complete
    UniqueClusters=np.unique(cluster_lables)
    
    Clustermeans_X =[]
    for i in UniqueClusters:
        #Clustermeans_X.append(np.percentile(X[cluster_lables==i],0.8,0)) # could use mean med or any other statistics
        Clustermeans_X.append(np.min(X[cluster_lables==i],0)) # could use mean med or any other statistics

    return Clustermeans_X


def my_kde_bandwidth(obj, fac=1./5):
    """We use Scott's Rule, multiplied by a constant factor."""
    return np.power(obj.n, -1./(obj.d+4)) * fac


def cleanHoughLines2(X,thresh=40.0):
    
    data = X[:,0]
    from scipy import stats
    from functools import partial
    kernel = stats.gaussian_kde(data,bw_method=partial(my_kde_bandwidth, fac=thresh))
    
    smoothed = kernel(data)
    
    #from scipy.signal import argrelmax
    #local_max_indx = argrelmax(smoothed)[0]
    local_max_indx = detect_local_minima(smoothed)
    
    
    Clustermeans_X = X[local_max_indx,:]

    return Clustermeans_X    
    
def cleanHoughLinesP(lines,distance=10):
    # removes lines close than distance with each other to remove the effect of canny edge detector 
    # that could capture both ends of thick lines as two lines 
    remove_list=[]
    for first_line in lines:
        x1,y1,x2,y2 = first_line[0]
        for index,second_line in enumerate(lines):
            x3,y3,x4,y4  = second_line[0]
            if y1==y2 and y3==y4: # Horizontal Lines
                diff = abs(y1-y3)
            elif x1==x2 and x3==x4: # Vertical Lines
                diff = abs(x1-x3)
            else:
                diff = 0

            if diff < distance and diff is not 0:
                remove_list.append(index)
    lines = np.delete(lines,remove_list,0)
    return lines
    
def remove_duplicates(lines):
    # remove duplicate lines (lines within 10 pixels of eachother)
    for x1, y1, x2, y2 in lines:
        for index, (x3, y3, x4, y4) in enumerate(lines):
            if y1 == y2 and y3 == y4:
                diff = abs(y1-y3)
            elif x1 == x2 and x3 == x4:
                diff = abs(x1-x3)
            else:
                diff = 0
            if diff < 10 and diff is not 0:
                del lines[index]
    return lines
    
def sort_line_list(lines):
    # sort lines into horizontal and vertical
    vertical = []
    horizontal = []
    for line in lines:
        if line[0] == line[2]:
            vertical.append(line)
        elif line[1] == line[3]:
            horizontal.append(line)
    vertical.sort()
    horizontal.sort(key=lambda x: x[1])
    return horizontal, vertical

def hough_transform_p(edges,orig_Img):
    hough_Img = orig_Img.copy()
    # open and process images
    #img = cv2.imread('imgs/'+image)
    #gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #edges = cv2.Canny(gray, 50, 150, apertureSize=3)

    # probabilistic hough transform
    lines = cv2.HoughLinesP(edges, 1, np.pi/180, 200, minLineLength=20, maxLineGap=999)[0].tolist()

    # remove duplicates
    lines = remove_duplicates(lines)

    # draw image
    for x1, y1, x2, y2 in lines:
        cv2.line(hough_Img, (x1, y1), (x2, y2), (0, 0, 255), 1)
        
    
    # sort lines into vertical & horizontal lists
    horizontal, vertical = sort_line_list(lines)

    # go through each horizontal line (aka row)
    rows = []
    for i, h in enumerate(horizontal):
        if i < len(horizontal)-1:
            row = []
            for j, v in enumerate(vertical):
                if i < len(horizontal)-1 and j < len(vertical)-1:
                    # every cell before last cell
                    # get width & height
                    width = horizontal[i+1][1] - h[1]
                    height = vertical[j+1][0] - v[0]

                else:
                    # last cell, width = cell start to end of image
                    # get width & height
                    width  = hough_Img.width()
                    height = hough_Img.height()
                

                # get roi (region of interest) to find an x
                roi = hough_Img[h[1]:h[1]+width, v[0]:v[0]+height]


                # if roi contains an x, add x to array, else add _
                roi_gry = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)
                ret, thresh = cv2.threshold(roi_gry, 127, 255, 0)
                contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

                if len(contours) > 1:
                    # there is an x for 2 or more contours
                    row.append('x')
                else:
                    # there is no x when len(contours) is <= 1
                    row.append('_')
            row.pop()
            rows.append(row)
    return hough_Img,rows


def PIL2OpenCVarray(PILimg):
    pil_image     = PILimg.convert('RGB')
    open_cv_image = np.array(pil_image) 
    # Convert RGB to BGR 
    open_cv_image = open_cv_image[:, :, ::-1].copy() 
    return open_cv_image

def OpenCVarray2PIL(OPENCVimg):
    trueimg = cv2.cvtColor(OPENCVimg, cv2.COLOR_BGR2RGB)
    PILimg = Image.fromarray(trueimg)
    
    return PILimg


def detect_local_minima(arr):
    import numpy as np
    import scipy.ndimage.filters as filters
    import scipy.ndimage.morphology as morphology

    # http://stackoverflow.com/questions/3684484/peak-detection-in-a-2d-array/3689710#3689710
    """
    Takes an array and detects the troughs using the local maximum filter.
    Returns a boolean mask of the troughs (i.e. 1 when
    the pixel's value is the neighborhood maximum, 0 otherwise)
    """
    # define an connected neighborhood
    # http://www.scipy.org/doc/api_docs/SciPy.ndimage.morphology.html#generate_binary_structure
    neighborhood = morphology.generate_binary_structure(len(arr.shape),2)
    # apply the local minimum filter; all locations of minimum value 
    # in their neighborhood are set to 1
    # http://www.scipy.org/doc/api_docs/SciPy.ndimage.filters.html#minimum_filter
    local_min = (filters.minimum_filter(arr, footprint=neighborhood)==arr)
    # local_min is a mask that contains the peaks we are 
    # looking for, but also the background.
    # In order to isolate the peaks we must remove the background from the mask.
    # 
    # we create the mask of the background
    background = (arr==0)
    # 
    # a little technicality: we must erode the background in order to 
    # successfully subtract it from local_min, otherwise a line will 
    # appear along the background border (artifact of the local minimum filter)
    # http://www.scipy.org/doc/api_docs/SciPy.ndimage.morphology.html#binary_erosion
    eroded_background = morphology.binary_erosion(
        background, structure=neighborhood, border_value=1)
    # 
    # we obtain the final mask, containing only peaks, 
    # by removing the background from the local_min mask
    detected_minima = local_min - eroded_background
    return np.where(detected_minima)  




def cell_OCR(Im_orig , vertical, horizontal):
    import itertools
    
    # sort the lines
    vertical   = np.sort(vertical,0)
    horizontal = np.sort(horizontal,0)
    # go through each horizontal line (aka row)
    txt_out=[]
    shp = np.shape(Im_orig)
    big_image = 255.0*np.ones((shp[0],shp[1]),dtype=np.uint8)
    for i, h in enumerate(horizontal):
        if i < len(horizontal)-1:
            for j, v in enumerate(vertical):
                if i < len(horizontal)-1 and j < len(vertical)-1:
                    # every cell before last cell
                    # get width & height
                    width  = horizontal[i+1][0] - h[0]
                    height = vertical[j+1][0] - v[0]

                else:
                    # last cell, width = cell start to end of image
                    # get width & height
                    width  = 1
                    height = 1
                    #width  = np.shape(Im_orig)[0] - h[0]
                    #height = np.shape(Im_orig)[1] - v[0]
                
                if width*height<=3:
                    continue
                
                    

                # get roi (region of interest) to find an x
                roi = Im_orig[int(h[0]):int(h[0]+width), int(v[0]):int(v[0]+height)]

                
                
                if i<1 or i>=2:
                    continue
                
                
                
                '''
                if not (j==15 or j==16 or j==17):
                    continue
                '''
                
                '''
                if i==18:
                    print "hi"
                else:
                    continue
                '''
                
                txtocr   =[]
                OCRed_img=[]
                
                
                filterLevels = [[6],[3],[0]]
                filterLevels = list(itertools.product(*filterLevels))
                for diskdim,kerneldim,pyrsize in filterLevels:
                
                    OCRPreProced_binimg = PreProcess_ImageCell_For_OCR(roi,diskdim,kerneldim,pyrsize)
                    txt                 = pytesser.multi_image_to_strings([OCRPreProced_binimg] , cleanup = True)
                     
                    #==== OCR with Tesseract
                    OCRed_img.append(OCRPreProced_binimg)
                    txtocr.append(txt[0])
                
                
                
                uniq_txtocr = list(set(txtocr))
                uniq_conts = [txtocr.count(s) for ix,s in enumerate(uniq_txtocr)]
                orig_conts = [txtocr.count(s) for ix,s in enumerate(txtocr)]
                
                
                #==== calc the weights for ocred texts
                alpha_weights=[]
                for ix,t in enumerate(txtocr):
                    if t.isalpha()==True or t.isdigit()==True: # or t=='':
                        alpha_weights.append(2)
                    else:
                        alpha_weights.append(1)
                orig_conts = np.multiply(alpha_weights,orig_conts)
                     
                
                #==== find the max lik solution
                indx  = np.argmax(orig_conts)
                txt   = txtocr[indx]
                
                '''
                if txt.isdigit()==False and txt.isalpha()==False:
                    txt=''
                '''
                
                #print "min counts: "+str(np.min(uniq_conts))
                #print "max counts: "+str(np.max(uniq_conts))
                print uniq_conts 
                print uniq_txtocr
                print "selected scale: "+str(filterLevels[indx])
                print "selected OCR: " + txt
                
                
                
                
                annot_img = OCRed_img[0].copy()
                img_ocr   = OCRed_img[0].copy()
                
                
                #===== put the text on image
                cv2.putText(annot_img,txt, (20,20),cv2.FONT_HERSHEY_SIMPLEX, 1,(0,0,255),2)
                    
                dir = 'table%s' % (1)
                if not os.path.exists(dir):
                       os.makedirs(dir)
                fn1 = '%s/roi_r%s-c%s.tiff' % (dir, i, j)
                
                dir = 'table%s' % (2)
                if not os.path.exists(dir):
                       os.makedirs(dir)
                fn2 = '%s/roi_r%s-c%s.tiff' % (dir, i, j)
                
                #if txt!='':
                #tiff.imsave(fn1, annot_img)
                #tiff.imsave(fn2, img_ocr)
                
                cv2.imwrite(fn1, annot_img)
                cv2.imwrite(fn2, img_ocr )
                
                
                row_image[] = img_ocr.copy()
                big_image[int(h[0]):int(h[0]+width), int(v[0]):int(v[0]+height)] = img_ocr.copy()
                loc = '%s/bigimg.tiff' % (dir)
                
                txt_out.append(txt)
                
    cv2.imwrite(loc, big_image)            
    print txt_out
                
                    
                    
                
                
def trim(in_npArray):
    from PIL import Image, ImageChops
    im = Image.fromarray(in_npArray)
    bg = Image.new(im.mode, im.size, im.getpixel((0,0)))
    diff = ImageChops.difference(im, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        return np.uint8(np.asarray(im.crop(bbox)))
    else:
        return in_npArray
        
def trim_borders(in_Array):
    width  = np.shape(in_Array)[0]
    height = np.shape(in_Array)[1]
    crw = 5
    crh = 5
    out_Array = in_Array[crw:width-crw , crh:height-crh]
    return out_Array
         
def PreProcess_ImageCell_For_OCR(roi,diskdim,kerneldim=4,pyrsize=0):
     out_Grey_ImageNpArray = roi.copy()
     thresh = roi.copy()
     
     #roi         = Background_subtraction(roi.copy())
    
     roi_gry     = cv2.cvtColor(roi.copy(), cv2.COLOR_BGR2GRAY)
     thresh      = roi_gry.copy()
     ret, thresh = cv2.threshold(thresh.copy(), 110 , 255, cv2.THRESH_BINARY) 
     
     #thresh      = median(thresh.copy(), disk(4))
     
     thresh      = whiten_borders(thresh.copy() , thickness=20)
     
     #=== erode the text (make thicker)
     kernel = np.ones((kerneldim,kerneldim),np.uint8)
     eroded = cv2.erode(thresh.copy(),kernel,iterations = 2)
     thresh = eroded.copy()
     
     ''' This line was not used in training but makes the results better'''
      
     
     
     '''
     #=== erosion to make the numbers and text thinner 
     kernel = np.ones((kerneldim,kerneldim),np.uint8)
     #eroded = cv2.dilate(thresh.copy(),kernel,iterations = 1)
     eroded = cv2.erode(~thresh.copy(),kernel,iterations = 1)
     thresh = ~eroded.copy()
     '''
     

     
     ''''
     #=== Use image pyramids to downsample the image
     h,w = thresh.shape[:2]
     im_pyr = thresh.copy()
     for psize in range(pyrsize):
         im_pyr = cv2.pyrDown(im_pyr.copy())
     thresh = im_pyr.copy()
     '''
     
     
     Im_autocanny = auto_canny(thresh.copy())
     kernel       = cv2.getStructuringElement(cv2.MORPH_CROSS,(30,30))
     dilated      = cv2.dilate(Im_autocanny,kernel,iterations = 10) # dilate
     im2, contours, hierarchy = cv2.findContours(dilated,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE) 
     
     try:
         cnt     = contours[0]
         x,y,w,h = cv2.boundingRect(cnt)
         temp = np.uint8(255.*np.ones(np.shape(thresh)))
         temp[y:y+h,x:x+w]  = thresh[y:y+h,x:x+w]
     except:
         "just take the threshold image"
     
     
     
     
     
     
     
     
     '''
     # for each contour found, draw a rectangle around it on original image
     img_out=[]
     for contour in contours:
            # get rectangle bounding contour
            [x,y,w,h] = cv2.boundingRect(contour)
    
            # discard areas that are too small
            if h<8 or w<8:
                continue
        
            # draw rectangle around contour on original image
            #cv2.rectangle(thresh,(x,y),(x+w,y+h),(255,0,255),2)
            img_out.append(thresh[y:y+h,x:x+w])
            
       
     
     '''
     
     #thresh      = trim(thresh.copy())

     
     out_Grey_ImageNpArray = thresh.copy()
     #del thresh , thresh1 , roi_gry
     return out_Grey_ImageNpArray
                   



def Background_subtraction(orig_color_img):

    #== Parameters =======================================================================
    BLUR = 21
    CANNY_THRESH_1 = 10
    CANNY_THRESH_2 = 200
    MASK_DILATE_ITER = 10
    MASK_ERODE_ITER = 10
    MASK_COLOR = (0.0,0.0,1.0) # In BGR format
    
    
    #== Processing =======================================================================
    
    #-- gray image -----------------------------------------------------------------------
    gray = cv2.cvtColor(orig_color_img,cv2.COLOR_BGR2GRAY)
    
    #-- Edge detection -------------------------------------------------------------------
    edges = cv2.Canny(gray, CANNY_THRESH_1, CANNY_THRESH_2)
    edges = cv2.dilate(edges, None)
    edges = cv2.erode(edges, None)
    
    #-- Find contours in edges, sort by area ---------------------------------------------
    contour_info = []
    contours, _ = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    for c in contours:
        contour_info.append((
            c,
            cv2.isContourConvex(c),
            cv2.contourArea(c),
        ))
    contour_info = sorted(contour_info, key=lambda c: c[2], reverse=True)
    max_contour = contour_info[0]
    
    #-- Create empty mask, draw filled polygon on it corresponding to largest contour ----
    # Mask is black, polygon is white
    mask = np.zeros(edges.shape)
    cv2.fillConvexPoly(mask, max_contour[0], (255))
    
    #-- Smooth mask, then blur it --------------------------------------------------------
    mask = cv2.dilate(mask, None, iterations=MASK_DILATE_ITER)
    mask = cv2.erode(mask, None, iterations=MASK_ERODE_ITER)
    mask = cv2.GaussianBlur(mask, (BLUR, BLUR), 0)
    mask_stack = np.dstack([mask]*3)    # Create 3-channel alpha mask
    
    #-- Blend masked img into MASK_COLOR background --------------------------------------
    mask_stack      = mask_stack.astype('float32') / 255.0          # Use float matrices, 
    orig_color_img  = orig_color_img.astype('float32') / 255.0                 #  for easy blending
    
    masked = (mask_stack * orig_color_img) + ((1-mask_stack) * MASK_COLOR) # Blend
    masked = (masked * 255).astype('uint8')                     # Convert back to 8-bit 
    
    #cv2.imshow('img', masked)                                   # Display
    #cv2.waitKey()
    
    #cv2.imwrite('C:/Temp/person-masked.jpg', masked)           # Save
    return masked



#### imclearborder definition
def whiten_borders(img , thickness):
    img_out = img.copy()
    (h,w) = np.shape(img)
    
    if ~(h<thickness or w<thickness):
        # whiten the around area
        img_out[0:thickness-1,:]=255.
        img_out[-thickness+1:-1,:]=255.
        img_out[:,0:thickness-1]=255.
        img_out[:,-thickness+1:-1]=255.
        
    return img_out
    
def imclearborder(imgBW, radius):
    
    # Given a black and white image, first find all of its contours
    imgBWcopy = imgBW.copy()
    _,contours,hierarchy = cv2.findContours(imgBWcopy.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    # Get dimensions of image
    imgRows = imgBW.shape[0]
    imgCols = imgBW.shape[1]    

    contourList = [] # ID list of contours that touch the border

    # For each contour...
    for idx in np.arange(len(contours)):
        # Get the i'th contour
        cnt = contours[idx]

        # Look at each point in the contour
        for pt in cnt:
            rowCnt = pt[0][1]
            colCnt = pt[0][0]

            # If this is within the radius of the border
            # this contour goes bye bye!
            check1 = (rowCnt >= 0 and rowCnt < radius) or (rowCnt >= imgRows-1-radius and rowCnt < imgRows)
            check2 = (colCnt >= 0 and colCnt < radius) or (colCnt >= imgCols-1-radius and colCnt < imgCols)

            if check1 or check2:
                contourList.append(idx)
                break

    for idx in contourList:
        cv2.drawContours(imgBWcopy, contours, idx, (0,0,0), -1)

    return imgBWcopy

def rotatedRectWithMaxArea(w, h, angle):
  import math
  """
  Given a rectangle of size wxh that has been rotated by 'angle' (in
  radians), computes the width and height of the largest possible
  axis-aligned rectangle (maximal area) within the rotated rectangle.
  """
  if w <= 0 or h <= 0:
    return 0,0

  width_is_longer = w >= h
  side_long, side_short = (w,h) if width_is_longer else (h,w)

  # since the solutions for angle, -angle and 180-angle are all the same,
  # if suffices to look at the first quadrant and the absolute values of sin,cos:
  sin_a, cos_a = abs(math.sin(angle)), abs(math.cos(angle))
  if side_short <= 2.*sin_a*cos_a*side_long:
    # half constrained case: two crop corners touch the longer side,
    #   the other two corners are on the mid-line parallel to the longer line
    x = 0.5*side_short
    wr,hr = (x/sin_a,x/cos_a) if width_is_longer else (x/cos_a,x/sin_a)
  else:
    # fully constrained case: crop touches all 4 sides
    cos_2a = cos_a*cos_a - sin_a*sin_a
    wr,hr = (w*cos_a - h*sin_a)/cos_2a, (h*cos_a - w*sin_a)/cos_2a

  return wr,hr
  
  
def Crop_Adaptive_Image(horiz_lines , vert_lines):
    
    #=== Get the Proper Zone
    horiz_lines = np.sort(horiz_lines,axis=0)
    templines = horiz_lines[:,0]
    aa        = np.diff(templines,n=1)
    bb_norm   = aa/np.median(aa)
    bb_bin    = [(s>=0.9 and s<=1.1) for s in bb_norm] # is jump close to the median jump in line differences?
    reg       = contiguous_regions(bb_bin) # get continuese regions of True value
    reg_maxindx    = np.argmax(np.diff(reg))
    Horiz_crop_reg = reg[reg_maxindx,:]
    
    # remove lines outside of this region
    horiz_lines = horiz_lines[Horiz_crop_reg[0]:Horiz_crop_reg[1]+1,:]
    
    del templines , aa ,bb_norm ,bb_bin ,reg ,reg_maxindx
    return horiz_lines , vert_lines
    
    
def contiguous_regions(condition):
    """Finds contiguous True regions of the boolean array "condition". Returns
    a 2D array where the first column is the start index of the region and the
    second column is the end index."""

    # Find the indicies of changes in "condition"
    d = np.diff(condition)
    idx, = d.nonzero() 

    # We need to start things after the change in "condition". Therefore, 
    # we'll shift the index by 1 to the right.
    idx += 1

    if condition[0]:
        # If the start of condition is True prepend a 0
        idx = np.r_[0, idx]

    if condition[-1]:
        # If the end of condition is True, append the length of the array
        idx = np.r_[idx, condition.size] # Edit

    # Reshape the result into two columns
    idx.shape = (-1,2)
    return idx

